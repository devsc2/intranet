<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;
use Crypt;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProspectExport;
use DB,
    App\Jobs\CreateFicheClient,
    DataTables;
use App\User;
use Auth;
use File;
use Storage;
class ProspectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export($id){
        return Excel::download(new ProspectExport($id), 'fiche-'.$id.'.xlsx');
    }

    protected function index(Request $request){
        $status = DB::table('status')->orderBy('order')->pluck('name', 'id')->toArray();

        $fournisseurs = DB::table('fournisseurs')->groupby('id')->pluck('name', 'id')->toArray();

        $fournisseurs_types = DB::table('fournisseurs_types')->pluck('name', 'id')->toArray();

        $fournisseurs_versions = DB::table('fournisseurs_version')->pluck('name', 'id')->toArray();

        $fournisseurs_produits_types = DB::table('fournisseurs_produits_types')->pluck('name', 'id')->toArray();

        $fournisseurs_produits_types_params = DB::table('fournisseurs_produits_types_params')->pluck('name', 'id')->toArray();

        $fournisseurs_dimensions = DB::table('fournisseurs_produits')
        ->join('fournisseurs_produits_dimensions', 'fournisseurs_produits_dimensions.id', '=', 'fournisseurs_produits.id_dimension')
        ->where('id_produit_type', 3)->pluck('fournisseurs_produits_dimensions.name', 'fournisseurs_produits_dimensions.id')->toArray();
        
        $allInstallateur = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.name', 'Installateur')
        ->select('users.id', 'users.name', 'users.email')
        ->get()->toArray();

        $allAssist = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.name', 'Assistante sociale')
        ->select('users.id', 'users.name', 'users.email')
        ->get()->toArray();

        $home_status = DB::table('prospect_home_status')
        ->select('id', 'name')
        ->get()->toArray();

        $sources = DB::table('prospect_sources')->get()->toArray();
	
        return view('prospects.prospect', ['status' => $status, 'fournisseurs' => $fournisseurs, 'fournisseurs_types' => $fournisseurs_types, 'fournisseurs_versions' => $fournisseurs_versions, 'fournisseurs_produits_types' => $fournisseurs_produits_types, 'fournisseurs_produits_types_params' => $fournisseurs_produits_types_params, 'fournisseurs_dimensions' => $fournisseurs_dimensions, 'installateurs' => $allInstallateur, 'assists' => $allAssist, 'home_status' => $home_status, 'sources' => $sources]);
    }

    protected function home_status($id){
        
        $data = DB::table('home_status_get_requirements')
        ->join('prospect_home_status_requirements', 'prospect_home_status_requirements.id', '=', 'home_status_get_requirements.id_require')
        ->leftjoin('prospect_home_status_requirements_params', 'prospect_home_status_requirements_params.id_require', '=', 'prospect_home_status_requirements.id')
        ->where('home_status_get_requirements.id_home_status', $id)
        ->select('prospect_home_status_requirements.name AS require')
        ->selectRaw("GROUP_CONCAT(prospect_home_status_requirements_params.name SEPARATOR ', ') AS params")
        ->groupby('home_status_get_requirements.id')->get()->toArray();
        return $data;
    }
    
    protected function getDimensions(Request $request){
        $id_fournisseur = $request->get('id_fournisseur');
        $id_type = $request->get('id_type');
        $id_version = $request->get('id_version');
        $id_produit_type = $request->get('id_produit_type');
        $id_produit_type_param = $request->get('id_produit_type_param');
        return DB::table('fournisseurs_produits')
        ->join('fournisseurs_produits_dimensions', 'fournisseurs_produits_dimensions.id', '=', 'fournisseurs_produits.id_dimension')
        ->where('fournisseurs_produits.id_produit_type', $id_produit_type)
        ->where('fournisseurs_produits.id_fournisseur', $id_fournisseur)
        ->where('fournisseurs_produits.id_type', $id_type)
        ->where('fournisseurs_produits.id_version', $id_version)
        ->where('fournisseurs_produits.id_produit_type_param', $id_produit_type_param)
        ->pluck('fournisseurs_produits_dimensions.name', 'fournisseurs_produits_dimensions.id')->toArray();
    }

    protected function getInfos(Request $request){
        $id_fournisseur = $request->id_fournisseur;
        $id_type = $request->get('id_type');
        $id_version = $request->get('id_version');
        $id_produit_type = $request->get('id_produit_type');
        $id_produit_type_param = $request->get('id_produit_type_param');
        $id_dimension = $request->get('id_dimension');
        $data = DB::table('fournisseurs_produits')
        ->select(
            'id AS id',
            'référence')
        ->where('id_fournisseur', $id_fournisseur)
        ->where('id_type', $id_type)
        ->where('id_version', $id_version)
        ->where('id_produit_type_param', $id_produit_type_param)
        ->where('id_produit_type', $id_produit_type)
        ->where('id_dimension', $id_dimension)
        ->get()->toArray();

        return $data;
    }

    protected function create(Request $request){
        $dates = (array) json_decode($request->dates);
        if($request->source !== null){
            DB::table('prospect_sources')->insertOrIgnore([
                'name' => $request->source
            ]);
        }

        if(isset($request->decomposeFournisseur)){
            if(!DB::table('fournisseurs_produits')->where('référence',$request->decomposeFournisseur)->get()->first()){
                $request->id_reference = DB::table('fournisseurs_produits')->insertgetId([
                    "id_fournisseur" => 2,
                    "référence" => $request->decomposeFournisseur
                ]);
            }else{
                $request->id_reference = DB::table('fournisseurs_produits')->where('référence',$request->decomposeFournisseur)->pluck('id');
            }
            
        }
    
        DB::table('prospects')->insertOrIgnore([
            "id_folder_status" => $request->folderStatus,
            "id_home_status" => $request->status,
            'id_source' => DB::table('prospect_sources')->where('name', $request->source)->pluck('id')->first(),
            "id_reference" => $request->id_reference,
            "sexe" => $request->sexe,
            "full_name" => $request->full_name,
            "home_phone" => $request->fixePhoneNumber,
            "personal_phone" => $request->phoneNumber,
            "stairs" => $request->stairs,
            "devis" => $request->devisAnnexe,
            "mail_al" => $request->mailAL,
            "password_al" =>  $request->passwordAL,
            "address" => $request->address,
            "zip_code" => $request->zip_code,
            "city" => $request->city,
            "descriptif" => $request->descriptif,
            "commentaire" => $request->commentary,
        ]);

        DB::table('prospect_referent')->insertOrIgnore([
            "id_prospect" => DB::table('prospects')->max('id'),
            "firstname" => $request->referentFirstName,
            "lastname" => $request->referentLastName,
            "phone_number" => $request->referentPhoneNumber,
            "etat_matrimonial" => $request->referentStatus,
        ]);

        foreach ($request->users as $key => $value) {
            if($value !== null){
                DB::table('prospect_users')->insertOrIgnore([
                    'id_prospect' => DB::table('prospects')->max('id'),
                    'id_user' => $value
                ]);
            }
        }

        if(!empty($dates)){
            $newDates = [];
        
            foreach($dates as $key => $value){
                $value = (array) $value;
                $newDates[] = [
                    "id_prospect" => DB::table('prospects')->max('id'),
                    "name" => $value['date_name'], 
                    "date_appel" => $value['date_appel'], 
                    "date_intervention" => $value['date_intervention'],
                    "date_assist_social" => $value['date_assist']
                ];
            }
            DB::table('prospect_dates')->insertOrIgnore($newDates);
        }

        if(!empty($request->requirementsFile)){
            $validator = Validator::make($request->requirementsFile, [ 
                '*'=>'required|mimes:jpeg,jpg,png,pdf|max:6000'
            ]);
            if ($validator->fails()) {
                $response=array('status'=>'error','result'=>0,'errors'=>implode(',',$validator->errors()->all()));
                return response()->json($response, 200);
           }
            foreach ($request->requirementsFile as $key => $value) {
                $paths = explode('|', $key);
                $path = implode('/', $paths);
                $value->storeAs('public/prospects/requirements/'.DB::table('prospects')->max('id').'/'.$path, end($paths).'.'.$value->getClientOriginalExtension());
                DB::table('prospect_documents')->insert(['id_prospect' => DB::table('prospects')->max('id'),
                'name' => end($paths),
                'extension' => $value->getClientOriginalExtension(),
                'path' => $path,
                'size' => $value->getSize()]);
            }
        }
            
        foreach (json_decode($request->signs) as $key => $value) {
            if(!empty($value)){
                $encoded_image = explode(",", $value)[1];
                $decoded_image = base64_decode($encoded_image);
                Storage::put('/public/prospects/requirements/'.DB::table('prospects')->max('id').'/Signatures/'.$key.'/'.$key.'.png', $decoded_image);
                DB::table('prospect_documents')->insert(['id_prospect' => DB::table('prospects')->max('id'),
                'name' => $key,
                'extension' => 'png',
                'path' => 'Signatures/'.$key,
                'size' => null]);
            }
        }
        return true;
    }

    protected function readAll(Request $request){
	
        $idUser = auth()->user()->id;
        if(Auth::user()->hasAnyRoles(['Installateur', 'Assistante sociale'])){
            $data = DB::table('prospects')
            ->select('prospects.id', 'prospects.full_name', 'status.name as statusName', 'prospect_home_status.name as home_status', 'prospects.personal_phone', 'prospects.home_phone', 'prospects.stairs', 'prospects.mail_al', 'prospects.zip_code', 'prospects.city', 'prospects.sexe', 'prospects.address', 'prospect_sources.name AS source', 'fournisseurs_produits.référence')
            ->orderBy('status.order')
            ->join('status', 'status.id', '=', 'prospects.id_folder_status')
            ->join('prospect_users', 'prospect_users.id_prospect', '=', 'prospects.id')
            ->leftJoin('users', 'users.id', '=', 'prospect_users.id_user')
            ->leftjoin('prospect_sources', 'prospect_sources.id', '=', 'prospects.id_source')
            ->join('prospect_home_status', 'prospect_home_status.id', '=', 'prospects.id_home_status')
            ->leftjoin('fournisseurs_produits', 'fournisseurs_produits.id', '=', 'prospects.id_reference')
            ->distinct()
            ->where('users.id', $idUser)->get()->toArray();
        }else{
            $data = DB::table('prospects')
            ->select('prospects.id', 'prospects.full_name', 'status.name as statusName', 'prospect_home_status.name as home_status', 'prospects.personal_phone', 'prospects.home_phone', 'prospects.stairs', 'prospects.mail_al', 'prospects.zip_code', 'prospects.city', 'prospects.address', 'prospect_sources.name AS source', 'fournisseurs_produits.référence')
            ->orderBy('status.order')
            ->join('status', 'status.id', '=', 'prospects.id_folder_status')
            ->leftjoin('prospect_sources', 'prospect_sources.id', '=', 'prospects.id_source')
            ->join('prospect_home_status', 'prospect_home_status.id', '=', 'prospects.id_home_status')
            ->leftjoin('fournisseurs_produits', 'fournisseurs_produits.id', '=', 'prospects.id_reference')
            ->get()->toArray();
        }
        return DataTables::of($data)->make(true);
    }

    protected function read(Request $request, $id){
        if(Auth::user()->hasAnyRoles(['Installateur', 'Assistante sociale'])){
            $idUser = auth()->user()->id;
            if(!DB::table('prospect_users')->where('id_prospect', $id)->where('id_user', $idUser)->first()){
                return redirect()->back()->with('error', 'Vous n\'avez pas accès à cette fiche');
            };
        }

        $installateur = DB::table('prospect_users')
        ->join('users', 'users.id', '=', 'prospect_users.id_user')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('prospect_users.id_prospect', $id)
        ->where('roles.name', 'Installateur')
        ->select('roles.id AS roleId', 'users.id', 'roles.name AS role', 'users.name', 'users.email')->first();

        $assist = DB::table('prospect_users')
        ->join('users', 'users.id', '=', 'prospect_users.id_user')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('prospect_users.id_prospect', $id)
        ->where('roles.name', 'Assistante sociale')
        ->select('roles.id AS roleId', 'users.id', 'roles.name AS role', 'users.name', 'users.email')->first();

        $ref = DB::table('fournisseurs_produits')
        ->leftjoin('prospects', 'fournisseurs_produits.id', '=', 'prospects.id_reference')
        ->leftjoin('fournisseurs_produits_dimensions', 'fournisseurs_produits_dimensions.id', '=', 'fournisseurs_produits.id_dimension')
        ->leftjoin('fournisseurs', 'fournisseurs.id', '=', 'fournisseurs_produits.id_fournisseur')
        ->leftjoin('fournisseurs_produits_types', 'fournisseurs_produits_types.id', '=', 'fournisseurs_produits.id_produit_type')
        ->leftjoin('fournisseurs_produits_types_params', 'fournisseurs_produits_types_params.id', '=', 'fournisseurs_produits.id_produit_type_param')
        ->leftjoin('fournisseurs_types', 'fournisseurs_types.id', '=', 'fournisseurs_produits.id_type')
        ->where('prospects.id', $id)
        ->select('fournisseurs.name AS fournisseur', 'fournisseurs_produits.id', 'fournisseurs_produits_dimensions.name AS dimension', 'fournisseurs_types.name as type', 'fournisseurs_produits_types_params.name as Pparams', 'fournisseurs_produits_types.name as ProduitType', 'fournisseurs_produits.référence As ref')->first();

        $data = DB::table('prospects')
        ->select('prospects.full_name AS pfullname', 'prospects.address', 'prospects.zip_code', 'prospects.city', 'prospects.personal_phone AS pPhoneNumber', 'prospects.home_phone', 'prospect_referent.firstname AS RFirstname', 'prospect_referent.lastname AS RLastname', 'prospect_referent.phone_number AS RPhoneNumber', 'status.name AS status',
        'prospect_home_status.name AS home_status', 'prospects.sexe', 'prospects.id_home_status', 'prospects.stairs', 'prospects.mail_al', 'prospects.password_al', 'prospects.devis', 'prospects.descriptif', 'prospects.commentaire', 'prospect_sources.name AS source', 'prospect_referent.etat_matrimonial', 'fournisseurs_produits.référence', 'fournisseurs_produits.id as RefId')
        ->leftjoin('status', 'status.id', '=', 'prospects.id_folder_status')
        ->leftjoin('prospect_referent', 'prospect_referent.id_prospect', '=', 'prospects.id')
        ->leftjoin('prospect_home_status', 'prospect_home_status.id', '=', 'prospects.id_home_status')
        ->leftjoin('prospect_sources', 'prospect_sources.id', '=', 'prospects.id_source')
        ->leftjoin('fournisseurs_produits', 'fournisseurs_produits.id', '=', 'prospects.id_reference')
        ->where('prospects.id', $id)
        ->get()->toArray();

        $dates = DB::table('prospect_dates')
        ->select('id', 'name', 'date_appel', 'date_intervention', 'date_assist_social')
        ->where('id_prospect', $id)
        ->get()->toArray();


        if(Auth::user()->hasAnyRole('Installateur')){
            $docs = ['PHOTOS', 'PHOTOS1', 'PHOTOS2', 'PHOTOS3', 'PHOTOS4', 'PHOTOS5', 'FICHE METRAGE', 'PLAN SDB'];
            $documents = DB::table('prospect_documents')->select('id_prospect', 'path', 'name', 'extension')->where('id_prospect', $id)->whereIn('name', $docs)->get()->toArray();
        }else{
            $documents = DB::table('prospect_documents')
            ->select('id_prospect', 'path', 'name', 'extension')->where('id_prospect', $id)->get()->toArray();
        }
        
        foreach ($documents as $key => $value) {
            $documents[$key] = [implode(', ', explode('/', $value->path)),"prospects/requirements/".$value->id_prospect.'/'.$value->path.'/'.$value->name.'.'.$value->extension];
        }

        if($data){
            return view('prospects.read', ['ref' => $ref, 'data' => (array) $data[0], 'dates' => $dates, 'id' => $id, 'instas' => $installateur, 'assist' => $assist, 'documents' => $documents, 'test' => $this->home_status(intval(DB::table('prospects')->pluck('id_home_status')->first()))]);
        }else{
            return redirect('/prospect/read');
        }
    }

    protected function edit(Request $request, $id){
        $idUser = auth()->user()->id;

        if(Auth::user()->hasAnyRole('Assistante sociale')){
            if(!DB::table('prospect_users')->where('id_prospect', $id)->where('id_user', $idUser)->first()){
                return redirect()->back()->with('error', 'Vous n\'avez pas accès à cette fiche');
            };
        }
        
        $data = DB::table('prospects')
        ->select('prospects.full_name AS pfullname', 'prospects.address', 'prospects.zip_code', 'prospects.city', 'prospects.personal_phone AS pPhoneNumber', 'prospects.home_phone', 'prospect_referent.firstname AS RFirstname', 'prospect_referent.lastname AS RLastname', 'prospect_referent.phone_number AS RPhoneNumber', 'status.name AS status',
        'prospect_home_status.name AS home_status', 'prospects.sexe', 'prospects.stairs', 'prospects.mail_al', 'prospects.password_al', 'prospects.devis', 'prospects.descriptif', 'prospects.commentaire', 'prospect_sources.name AS source', 'prospect_referent.etat_matrimonial')
        ->join('status', 'status.id', '=', 'prospects.id_folder_status')
        ->join('prospect_home_status', 'prospect_home_status.id', '=', 'prospects.id_home_status')
        ->leftjoin('prospect_referent', 'prospect_referent.id_prospect', '=', 'prospects.id')
        ->leftjoin('prospect_sources', 'prospect_sources.id', '=', 'prospects.id_source')
        ->where('prospects.id', $id)
        ->get()->toArray();

        $ref = DB::table('fournisseurs_produits')
        ->leftjoin('prospects', 'fournisseurs_produits.id', '=', 'prospects.id_reference')
        ->leftjoin('fournisseurs_produits_dimensions', 'fournisseurs_produits_dimensions.id', '=', 'fournisseurs_produits.id_dimension')
        ->leftjoin('fournisseurs', 'fournisseurs.id', '=', 'fournisseurs_produits.id_fournisseur')
        ->leftjoin('fournisseurs_produits_types', 'fournisseurs_produits_types.id', '=', 'fournisseurs_produits.id_produit_type')
        ->leftjoin('fournisseurs_produits_types_params', 'fournisseurs_produits_types_params.id', '=', 'fournisseurs_produits.id_produit_type_param')
        ->leftjoin('fournisseurs_types', 'fournisseurs_types.id', '=', 'fournisseurs_produits.id_type')
        ->where('prospects.id', $id)
        ->select('fournisseurs.name AS fournisseur', 'fournisseurs_produits.id', 'fournisseurs_produits_dimensions.name AS dimension', 'fournisseurs_types.name as type', 'fournisseurs_produits_types_params.name as Pparams', 'fournisseurs_produits_types.name as        ProduitType', 'fournisseurs_produits.référence As ref')->first();

        $dates = DB::table('prospect_dates')
        ->select('id', 'name AS date_name', 'date_appel', 'date_intervention', 'date_assist_social AS date_assist')
        ->where('id_prospect', $id)
        ->get()->toArray();

        $installateur = DB::table('prospect_users')
        ->join('users', 'users.id', '=', 'prospect_users.id_user')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('prospect_users.id_prospect', $id)
        ->where('roles.name', 'Installateur')
        ->select('roles.id AS roleId', 'users.id', 'roles.name AS role', 'users.name', 'users.email')->first();

        $assist = DB::table('prospect_users')
        ->join('users', 'users.id', '=', 'prospect_users.id_user')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('prospect_users.id_prospect', $id)
        ->where('roles.name', 'Assistante sociale')
        ->select('roles.id AS roleId', 'users.id', 'roles.name AS role', 'users.name', 'users.email')->first();

        $sources = DB::table('prospect_sources')->get()->toArray();

        
        if(empty($users[1])){
            $users[1] = (object) [
                'role_id' => DB::table('roles')->where('name', 'Assistante sociale')->pluck('id')->first(),
                'id' => 0,
                'role' => 'Assistante sociale',
                'name' => null,
                'email' => null,
            ];
        }

        $allInstallateur = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.name', 'Installateur')
        ->select('users.id', 'users.name', 'users.email')->get()->toArray();
      
        $allAssist = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.name', 'Assistante sociale')
        ->select('users.id', 'users.name', 'users.email')->get()->toArray();

        $home_status = DB::table('prospect_home_status')
        ->select('id', 'name')
        ->get()->toArray();
        
        $address = DB::table('prospects')
        ->select('address', 'city', 'zip_code', 'country')
        ->where('id', $id)
        ->get()->toArray();

        $status = DB::table('status')->pluck('name', 'id')->toArray();
        $fournisseurs = DB::table('fournisseurs')->groupby('id')->pluck('name', 'id')->toArray();
        $fournisseurs_types = DB::table('fournisseurs_types')->pluck('name', 'id')->toArray();
        $fournisseurs_versions = DB::table('fournisseurs_version')->pluck('name', 'id')->toArray();
        $fournisseurs_produits_types = DB::table('fournisseurs_produits_types')->pluck('name', 'id')->toArray();
        $fournisseurs_produits_types_params = DB::table('fournisseurs_produits_types_params')->pluck('name', 'id')->toArray();
        $fournisseurs_dimensions = DB::table('fournisseurs_produits')
        ->join('fournisseurs_produits_dimensions', 'fournisseurs_produits_dimensions.id', '=', 'fournisseurs_produits.id_dimension')
        ->where('id_produit_type', 3)->pluck('fournisseurs_produits_dimensions.name', 'fournisseurs_produits_dimensions.id')->toArray();
        if(!empty($data)){
            return view('prospects.edit', ['ref' => $ref, 'id' => $id,'data' => (array) $data[0], 'dates' => $dates, 'status' => $status, 'fournisseurs' => $fournisseurs, 'fournisseurs_types' => $fournisseurs_types, 'fournisseurs_versions' => $fournisseurs_versions, 'fournisseurs_produits_types' => $fournisseurs_produits_types, 'fournisseurs_produits_types_params' => $fournisseurs_produits_types_params, 'fournisseurs_dimensions' => $fournisseurs_dimensions, 'address' => (array) $address[0], 'instas' => $installateur, 'assist' => $assist, 'installateurs' => $allInstallateur, 'allAssist' => $allAssist, 'home_status' => $home_status, 'docs' => $this->home_status(DB::table('prospects')->where('id', $id)->pluck('id_home_status')), 'sources' => $sources]);
        }else{
            return redirect('/prospect/read');
        }
    }

    protected function update(Request $request, $id){
        try{
            $newDates = (array) json_decode($request->get('dates'));

            if($request->source !== "undefined"){
                DB::table('prospect_sources')->insertOrIgnore([
                    'name' => $request->source
                ]);
            }
    
            DB::table('prospects')->where('id', $id)->update([
                "id_folder_status" => $request->get('folderStatus'),
                "id_source" => DB::table('prospect_sources')->where('name', $request->source)->pluck('id')->first()?:DB::table('prospects')->where('id', $id)->pluck('id_source')->first(),
                "sexe" => $request->sexe,
                "full_name" => $request->get('full_name'),
                "home_phone" => $request->get('fixePhoneNumber'),
                "personal_phone" => $request->get('phoneNumber'),
                "stairs" => $request->get("stairs"),
                "devis" => $request->get("devisAnnexe"),
                "address" => $request->address,
                "zip_code" => $request->zip_code,
                "city" => $request->city,
                "descriptif" => $request->get("descriptif"),
                "commentaire" => $request->get('commentary'),
            ]);
            if(DB::table('prospect_referent')->where('id_prospect', $id)->first()){
                DB::table('prospect_referent')->where('id', $id)->update([
                    "firstname" => $request->get('referentFirstName'),
                    "lastname" => $request->get('referentLastName'),
                    "phone_number" => $request->get('referentPhoneNumber'),
                    "etat_matrimonial" => $request->get('referentStatus'),
                ]);
            }else{
                DB::table('prospect_referent')->insert([
                    "id_prospect" => $id,
                    "firstname" => $request->get('referentFirstName'),
                    "lastname" => $request->get('referentLastName'),
                    "phone_number" => $request->get('referentPhoneNumber'),
                    "etat_matrimonial" => $request->get('referentStatus'),
                ]);
            }
            
    
            $oldDates = DB::table('prospect_dates')->where('id_prospect', $id)->select('name AS date_name', 'date_appel', 'date_intervention', 'date_assist_social AS date_assist')->get()->toArray();
                
            foreach ($newDates as $key => $value) {
                $newDates[$key] = json_encode((array) $value); 
            }
    
            foreach ($oldDates as $key => $value) {
                $oldDates[$key] = json_encode((array) $value);
            }
         
            $datesToDelete = array_diff($oldDates, $newDates);
            $datesToAdd = array_diff($newDates, $oldDates);
            
            foreach ($datesToDelete as $key => $value) {
                $value = json_decode($value);
                DB::table('prospect_dates')->where('id_prospect', $id)->where('date_appel', $value->date_appel)->where('date_intervention', $value->date_intervention)->delete();
            }
    
            foreach ($datesToAdd as $key => $value) {
                $value = (array) json_decode($value);
                $datesToAdd[$key] = [
                    'id_prospect' => $id,
                    'name' => $value['date_name'],
                    'date_appel' => $value['date_appel'],
                    'date_intervention' => $value['date_intervention'],
                    'date_assist_social' => $value["date_assist"]
                ];
            }
    
            DB::table('prospect_dates')->insertOrIgnore($datesToAdd);
    
            return true;
        }catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            return [$e->errorInfo[2]];
        }
        
    }

    protected function updateMail(Request $request, $id){
        DB::table('prospects')->where('id', $id)->update(['mail_al' => $request->get('mailAL')]);
        return redirect()->back()->with('success', 'Votre email à été modifier avec succès !');
    }

    protected function updatePwd(Request $request, $id){
        DB::table('prospects')->where('id', $id)->update(['password_al' => $request->get('passwordAL')]);
        return redirect()->back()->with('success', 'Votre mot de passe à été modifier avec succès !');
    }

    protected function updateReference(Request $request, $id){
        if(isset($request->decomposeFournisseur)){
            if(!DB::table('fournisseurs_produits')->where('référence',$request->decomposeFournisseur)->get()->first()){
                $request->id_reference = DB::table('fournisseurs_produits')->insertgetId([
                    "id_fournisseur" => 2,
                    "référence" => $request->decomposeFournisseur
                ]);
            }else{
                $request->id_reference = DB::table('fournisseurs_produits')->where('référence',$request->decomposeFournisseur)->pluck('id')->first();
            }
        }
        DB::table('prospects')->where('id', $id)->update(['id_reference' => $request->id_reference]);
        return redirect()->back()->with('success', 'La référence à bien été modifier !');
    }

    protected function updateficheInstall(Request $request, $idFiche, $idUser){
        if($idUser == 0){
            DB::table('prospect_users')->insert([
                'id_prospect' => intval($idFiche),
                'id_user' => intval($request->userChange),
            ]);
        }else{
            DB::table('prospect_users')
            ->where('prospect_users.id_prospect', intval($idFiche))
            ->where('prospect_users.id_user', intval($idUser))
            ->update(['prospect_users.id_user' => intval($request->userChange)]);
        }
        return redirect()->back()->with('success', 'La modification à bien été éffectuer');
    }

    protected function updateHomeStatus(Request $request, $id){
        DB::table('prospects')->where('id', $id)->update(['id_home_status' => $request->home_status]);
        return redirect()->back()->with('success', 'Le status maison à bien été modifier');
    }

    protected function updateDocuments(Request $request, $id){
        $validator = Validator::make($request->requirementsFile, [ 
            '*'=>'required|mimes:jpeg,jpg,png,pdf|max:20000'
        ]);
        if ($validator->fails()) {
            $response=array('status'=>'error','result'=>0,'errors'=>implode(',',$validator->errors()->all()));
            return redirect()->back()->with('error', implode(', ', array_keys($validator->messages()->messages())).' : fichier trop volumineux ou le format n\'est pas accepté. Formats acceptés : jpeg,jpg,png,pdf');
        }else if(!empty($request->requirementsFile)){
            foreach ($request->requirementsFile as $key => $value) {
                $paths = explode('|', $key);
                $path = implode('/', $paths);
                $value->storeAs('public/prospects/requirements/'.$id.'/'.$path, end($paths).'.'.$value->getClientOriginalExtension());
                if(DB::table('prospect_documents')->where('id_prospect', $id)->where('path', $path)->first()){
                    DB::table('prospect_documents')->where('id_prospect', $id)->where('path', $path)->update([
                    'name' => end($paths),
                    'extension' => $value->getClientOriginalExtension(),
                    'path' => $path,
                    'size' => $value->getSize()]);
                }else{
                    DB::table('prospect_documents')->insert([
                    'id_prospect' => $id,
                    'name' => end($paths),
                    'extension' => $value->getClientOriginalExtension(),
                    'path' => $path,
                    'size' => $value->getSize()]);
                }
            }
        }
        
        return redirect()->back()->with('success', 'Les documents ont bien été modifiés');
    }

    protected function updateSigns(Request $request, $id){
        if(!DB::table('prospect_documents')->where('id_prospect', $id)->where('name', $request->name)->first()){
            DB::table('prospect_documents')->insert([
            'id_prospect' => $id,
            'name' => $request->name,
            'extension' => '.png',
            'path' => 'Signatures/'.$request->name,
            'size' => null]);
        }
        $encoded_image = explode(",", $request->base64)[1];
        $decoded_image = base64_decode($encoded_image);
        Storage::put('/public/prospects/requirements/'.$id.'/Signatures/'.$request->name.'/'.$request->name.'.png', $decoded_image);
        return true;
    }

    protected function deleteDocs(Request $request, $id_prospect, $doc){
        $path = implode('/', explode(', ', $doc));
        File::deleteDirectory(storage_path('app/public/prospects/requirements/'.$id_prospect.'/'.$path));
        $file_name = DB::table('prospect_documents')->where('id_prospect', $id_prospect)->where('path', $path)->pluck('name')->first();
        DB::table('prospect_documents')->where('id_prospect', $id_prospect)->where('path', $path)->delete();
        return redirect()->back()->with('success', "Le fichier $file_name à bien été supprimer !");
    }

    protected function addDocs(Request $request, $id){
        $request->file("requirementsFile")->storeAs('public/prospects/requirements/'.$id.'/Fin_chantier/', 'Fin_de_chantier.'.$request->file('requirementsFile')->getClientOriginalExtension());
        return redirect()->back()->with('success', 'Documents de fin de chantier ajouter');
    }

    protected function destroy(Request $request, $id){
        DB::table('prospects')->where('id', $id)->delete();
        File::deleteDirectory(storage_path('app/public/prospects/requirements/'.$id));
        return redirect('/prospect/read')->with('success', 'La fiche numéro '.$id.' à bien été supprimer');
    }
}
