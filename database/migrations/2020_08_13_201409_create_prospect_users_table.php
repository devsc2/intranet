<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_prospect')->unsigned()->index();
            $table->integer('id_user')->unsigned()->index();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->unique(['id_prospect', 'id_user']);
        });

        Schema::table('prospect_users', function (Blueprint $table){
            $table->foreign('id_prospect')->references('id')->on('prospects')->onDelete('cascade');
        });

        Schema::table('prospect_users', function (Blueprint $table){
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_users');
    }
}
