<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with(['users' => User::all(), 'roles' => Role::all()]);
    }

    public function store(Request $request){
        if(!User::where('email', $request->email)->first()){
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            $user->roles()->attach($request->userRoles);
    
            return redirect()->back()->with('success', 'L\'utilisateur a bien été ajouter !');
        }
        return redirect()->back()->with('error', 'L\'utilisateur qui a pour mail : '.$request->email.' existe déjà !');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roles = $request->userRoles;
        $user = User::find($id);
        $user->update(['name' => $request->name, 'email' => $request->email]);
        $user->roles()->sync($roles);
        return redirect()->back()->with('success', 'L\'utilisateur a bien été modifier !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->back()->with('success', 'L\'utilisateur a bien été supprimer !');
    }
}
