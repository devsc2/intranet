<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sources = DB::table('prospect_sources')->get()->all();
        return view('admin.sources.index', ['sources' => $sources]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!DB::table('prospect_sources')->where('name', $request->name)->first()){
            DB::table('prospect_sources')->insert(['name' => $request->name]);
            return redirect()->back()->with('success', 'Le status '.$request->name.' a bien été créer !');
        }
        return redirect()->back()->with('error', 'La source '.$request->name.' existe déjà !');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('prospect_sources')->where('id', $id)->update(['name' => $request->name]);
        return redirect()->back()->with('success', 'La source à bien été modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('prospect_sources')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Votre source à bien été supprimer !');
    }
}
