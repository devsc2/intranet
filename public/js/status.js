var el = document.getElementById('table');
var dragger = tableDragger.default(el, {
    dragHandler: ".handle",
    mode: 'row'
}).on('drop', (from, to, el, mode) => {
    var object = {};
    for (let index = Math.min(from, to) - 1; index < Math.max(from, to); index++) {
        const newOrder = index + 1;
        const id = el.children[1].children[index].children[0].value;
        object[id] = newOrder;
        el.children[1].children[index].children[1].innerText = newOrder;
    }
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: '/admin/status/order',
        data: {object: object},
        dataType: 'json',
    })
})