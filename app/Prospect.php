<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Prospect extends Model
{
    protected $fillable = [
        'full_name', 'address', 'zip_code', 'city', 'home_phone', 'personal_phone', 'status', 'stairs', 'mail_al', 'password_al'
    ];

    protected $hidden = [
        'mail_al', 'password_al'
    ];
}
