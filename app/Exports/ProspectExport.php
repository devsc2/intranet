<?php

namespace App\Exports;

use App\Prospect;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;



class ProspectExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = Prospect::where('prospects.id', $this->id)
        ->join('status', 'status.id', '=', 'prospects.id_folder_status')
        ->join('prospect_dates', 'prospect_dates.id_prospect', '=', 'prospects.id')
        ->join('prospect_references', 'prospect_references.prospect_id', '=', 'prospects.id')
        ->join('prospect_referent', 'prospect_referent.id_prospect', '=', 'prospects.id')
        ->join('fournisseurs_produits', 'fournisseurs_produits.id', '=', 'prospect_references.id_reference')
        ->select('prospects.full_name', 'prospects.home_phone', 'prospects.personal_phone', 'status.name AS Status dossier', 'prospects.stairs', 'prospects.devis', 'prospects.mail_al', 'prospects.password_al', 'prospects.address', 'prospects.zip_code', 'prospects.city', 'prospects.country', 'prospects.descriptif', 'prospects.commentaire')
        ->selectRaw('GROUP_CONCAT(fournisseurs_produits.référence SEPARATOR ", ") AS ref')
        ->selectRaw('GROUP_CONCAT(prospect_dates.date_appel, " - ", prospect_dates.date_intervention SEPARATOR ", ") AS dates')
        ->groupByRaw('prospects.id')
        ->get();
        return $data;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Styling a specific cell by coordinate.
            'A1:P1' => ['font' => ['bold' => true]],
        ];
    }

    public function headings(): array
    {
        return [
            'Nom complet',
            'Téléphone fixe',
            'Téléphone portable',
            'Status dossier', 
            'Escalier :',
            'Devis :',
            'Adresse',
            'Code postal',
            'Ville',
            'Pays',
            'Descriptif',
            'Commentaire',
            'Références',
            'Date appel - Date intervention'
        ];
    }
}
