<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.status.index')->with('status', DB::table('status')->orderBy('order')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!DB::table('status')->where('name', $request->name)->first()){
            DB::table('status')->insert(['name' => $request->name, 'order' => DB::table('status')->max('order')+1]);
            return redirect()->back()->with('success', 'Le status '.$request->name.' a bien été créer !');
        }
        return redirect()->back()->with('error', 'Le status '.$request->name.' existe déjà !');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('status')->where('id', $id)->update(['name' => $request->name]);
        return redirect()->back()->with('success', 'Le status à bien été modifier');
    }

    public function updateOrder(Request $request){
        foreach ($request->object as $key => $value) {
            DB::table('status')->where('id', $key)->update(['order' => intval($value)]);
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = DB::table('status')->where('id', $id)->pluck('order')->first();
        if($order !== null){
            $array = DB::table('status')->where('order', '>', $order)->orderBy('order')->pluck('order', 'id')->toArray();
            foreach ($array as $key => $value) {
                DB::table('status')->where('id', $key)->update(['order' => $value - 1]);
            }
        }
        DB::table('status')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Votre status à bien été supprimer !');
    }
}
