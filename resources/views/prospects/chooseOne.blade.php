@extends('layouts.app')

@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Choisi une fiche client pour la regarder plus en détail !'),
        'img' => 'prospect-readAll-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-body">                            
                        @if (\Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {!! \Session::get('success') !!}
                            </div>
                        @elseif(\Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {!! \Session::get('error') !!}
                            </div>
                        @endif
                            <table id="list_table" class="table table-striped table-bordered display responsive nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th width="5%">Id</th>
                                    <th>Nom complet</th>
                                    <th>Status dossier</th>
                                    <th>Propriétaire</th>
                                    <th>Téléphone portable</th>
                                    <th>Téléphone fixe</th>
                                    <th>Adresse</th>
                                    <th>Code postal</th>
                                    <th>Ville</th>
                                    <th>Escalier</th>
                                    <th>Mail AL</th>
                                    <th>Source</th>
                                    <th>Référence</th>
                                    <th width="5%">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        @hasroles(['admin', 'user'])
                        <div class="btn-group my-3" role="group">
                            <a class="btn btn-primary text-white" href="{{route('prospect.create')}}">Créer une fiche client</a>
                        </div>
                        @endhasroles
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')

    </div>

@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/chooseOne.js?ver=1.1')}}"></script>
@endsection
