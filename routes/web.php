<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth', 'auth.multiple_roles:admin,user,assistante sociale', 'cors'])->group(function() {
	Route::post('/prospect/home_status/getRequirements/{id}', 'ProspectController@home_status');
	Route::post('/prospect/getInfos', 'ProspectController@getInfos');
	Route::post('/prospect/dimensions', 'ProspectController@getDimensions');
	Route::get('/prospect/edit/{id}', 'ProspectController@edit')->name('edit');
	Route::post('/prospect/edit/{id}', 'ProspectController@update')->name('update');
	Route::put('/prospect/edit/password/{id}', 'ProspectController@updatePwd')->name('updatePwd');
	Route::put('/prospect/edit/mail/{id}', 'ProspectController@updateMail')->name('updateMail');
	Route::put('/prospect/edit/reference/{id}', 'ProspectController@updateReference')->name('updateReference');
	Route::put('/prospect/edit/{id}/{idUser}', 'ProspectController@updateficheInstall')->name('updateficheInstall');
	Route::delete('/prospect/delete/{id}', 'ProspectController@destroy')->name('destroy');
	Route::post('/prospect/export/{id}', 'ProspectController@export')->name('prospect.export');
	Route::put('/prospect/home_status/{id}', 'ProspectController@updateHomeStatus')->name('updateHomeStatus');
	Route::put('/prospect/documents/{id}', 'ProspectController@updateDocuments')->name('updateDocuments');
	Route::get('/prospect/create', 'ProspectController@index')->name('prospect.create');
	Route::post('/prospect/create', 'ProspectController@create');
});

Route::middleware(['auth', 'auth.multiple_roles:admin,user'])->group(function() {
	Route::post('/prospect/updateSigns/{id}', 'ProspectController@updateSigns');
});

Route::middleware(['auth', 'auth.admin'])->namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
	Route::resource('/users', 'UserController', ['except' => ['show', 'create', 'edit']]);
	Route::resource('/status', 'StatusController', ['except' => ['show', 'edit', 'create']]);
	Route::resource('/sources', 'SourceController', ['except' => ['show', 'edit', 'create']]);
	Route::post('/status/order', 'StatusController@updateOrder');
});

Route::middleware('auth')->group(function() {
	Route::get('/home', function(){
		return redirect()->route('home');
	});
	Route::get('/prospect/read', function(){
		return view('prospects.chooseOne');
	})->name('home');
	Route::get('/', function() {
		return redirect()->route('home');
	});
	Route::delete('/prospect/deleteDocs/{id}/{doc}', 'ProspectController@deleteDocs')->name('deleteDocs');
	Route::post('/prospect/addDocs/{id}', 'ProspectController@addDocs')->name('addDocs');
	Route::post('/prospect/read', 'ProspectController@readAll');
	Route::get('/prospect/read/{id}', 'ProspectController@read')->name('read');
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	
});


