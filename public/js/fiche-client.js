
var dates = {};
var date_id = 0;

function requirements(id){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: `/prospect/home_status/getRequirements/${id}`,
        success: (res) => {
            $("#requirementsInputs").empty();
            res.forEach((element, key) => {               
                $("#requirementsInputs").append(`
                    <div class="row" id="row${key}">
                        <div class="col" id="col${key}">
                            <h2>${element.require}</h2>
                        </div>
                    </div>
                `);
                if(element.require == "PHOTOS"){
                    $(`#row${key}`).append(`
                    <div class="col-md-5 text-right">
                        <button class="btn btn-primary btn-round" type="button" onclick="addPhoto(${key})">Ajouter une photo</button>
                    </div>
                    `)
                }
                if(element.params){
                    const params = element.params.split(', ');
                    params.forEach((e, k) => {
                        $(`#col${key}`).append(`
                            <div class="form-group">
                                <div class="input-group mb-4">
                                    <label for="requirementsFile${k}">${e} :</label>
                                    <input type="file" class="form-control-file" id="requirementsFile${k}" name="requirementsFile[${element.require}|${e}]">
                                </div>
                            </div>
                        `)
                    })
                }else{
                    $(`#col${key}`).append(`
                        <div class="form-group">
                            <div class="input-group mb-4">
                                <input type="file" class="form-control-file" id="requirementsFile${key}" name="requirementsFile[${element.require}]">
                            </div>
                        </div>
                    `)
                }
            });
        }
    })
}
$("#myForm").on('submit', function(e) {
    e.preventDefault();
    $("#ficheSuccess").css({"display": "none"});
    const formData = new FormData($('#myForm')[0]);
    $("#checkboxAlert").css({"display": "none"});
    formData.set('signs', JSON.stringify({"client": clientSignPad.isEmpty()?[]:clientSignPad.toDataURL(), "metreur": metteurSignPad.isEmpty()?[]:metteurSignPad.toDataURL()}));
    formData.set('source', source?source:'');
    formData.set('dates', JSON.stringify(dates));
    $("#date_alert").css({"display": "none"});
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    $("#ficheInfo").css({"display": "block"});
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        enctype: "multipart/form-data",
        cache: false,
        url: '/prospect/create',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: res =>{
            window.location.replace('/prospect/read');
        }
    });
});


