<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adminRole = Role::where('name', 'Admin')->first();
        $userRole = Role::where('name', 'User')->first();
        $InstallateurRole = Role::where('name', 'Installateur')->first();
        $assitRole = Role::where('name', 'Assistante sociale')->first();
        
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
        ]);

        $user = User::create([
            'name' => 'User',
            'email' => 'user@user.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
        ]);

        $installateur = User::create([
            'name' => 'Installateur',
            'email' => 'installateur@installateur.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
        ]);

        $assist = User::create([
            'name' => 'Assistante sociale',
            'email' => 'assist@assist.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
        $installateur->roles()->attach($InstallateurRole);
        $assist->roles()->attach($assitRole);

    }
}
