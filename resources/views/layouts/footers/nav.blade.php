<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} <a href="{{route('home')}}" class="font-weight-bold ml-1" target="_blank">{{env('APP_NAME')}}</a> &amp;
            <a href="{{route('home')}}" class="font-weight-bold ml-1" target="_blank">SC2 Consulting</a>
        </div>
    </div>
</div>