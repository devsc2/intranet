<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_folder_status')->unsigned()->index()->default(1);
            $table->integer('id_home_status')->unsigned()->index()->default(1);
            $table->integer('id_source')->unsigned()->index()->nullable();
            $table->integer('id_reference')->unsigned()->index()->nullable();
            $table->string('sexe', 8)->default('Monsieur');
            $table->string('full_name')->Nullable();
            $table->string('home_phone')->nullable()->unique();
            $table->string('personal_phone')->nullable()->unique();
            $table->string('stairs', 3)->default('Non');
            $table->string('devis', 3)->default('Non');
            $table->string('mail_al')->nullable()->unique();
            $table->string('password_al')->nullable();
            $table->string('address')->nullable();
            $table->integer('zip_code')->Nullable();
            $table->string('city')->Nullable();
            $table->string('country')->nullable();
            $table->longText('descriptif')->nullable();
            $table->longText('commentaire')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        Schema::table('prospects', function (Blueprint $table){
            $table->foreign('id_reference')->references('id')->on('fournisseurs_produits')->onDelete('cascade');
        });

        Schema::table('prospects', function (Blueprint $table){
            $table->foreign('id_home_status')->references('id')->on('prospect_home_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
    }
}
