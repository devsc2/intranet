@extends('layouts.app')

@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Voici la fiche client ! Bonne lecture'),
        'img' => 'prospect-read-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                        <div class="col alert alert-success" id="ficheSuccess" role="alert" style="display: none">
                        Votre fiche client à été enregistrée avec succès !
                        </div>
                        <div class="align-items-center">
                          @if (\Session::has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {!! \Session::get('success') !!}
                                    </div>
                                @elseif(\Session::has('error'))
                                    <div class="alert alert-danger" role="alert">
                                        {!! \Session::get('error') !!}
                                    </div>
                                @endif
                            <div class="row">
                                <div class="col">
                                    <h1>Visualisation d'une fiche client</h1>
                                </div>
                                
                                <div class="d-flex justify-content-end align-items-center col-md-5 text-right">
                                  @hasroles(['admin', 'user', 'Assistante sociale'])
                                    <form action="{{route('prospect.export', $id)}}" method="post" class="mr-2">
                                      {{ csrf_field() }}
                                      <button type="submit" class="btn btn-secondary"><i class="ni ni-cloud-download-95"></i>
                                      </button>
                                    </form>
                                    <a href="{{route('edit', $id)}}" class="btn btn-info" id='changeMode'>Mode édition</a>
                                    @endhasroles
                                    <button class="btn btn-info btn-round" type="button" data-toggle="modal" data-target="#documentModal">Documents</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                            <h1 class="mb-4">Client :</h1>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <h3>Sexe :</h3>
                                  <p class="form-control">{{$data['sexe']}}</p>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                  <h3>Nom complet :</h3>
                                <div class="form-group">
                                    <p class="form-control">{{$data['pfullname']}}</p>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Adresse :</h3>
                                <div class="form-group" id="locationField">
                                    <p class="form-control">{{$data['address']}}</p>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Code postal :</h3>
                                <div class="form-group">
                                  <p class="form-control">{{$data['zip_code']}}</p>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Ville :</h3>
                                <div class="form-group">
                                  <p class="form-control">{{$data['city']}}</p>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Téléphone fixe :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                                    </div>
                                    <p class="form-control">{{$data['home_phone']}}</p>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                  <h3>Téléphone portable :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <p class="form-control">{{$data['pPhoneNumber']}}</p>
                                    <div class="input-group-append">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <h1 class="mb-4">Référent :</h1>
                            <hr class="my-3">
                            <div class="row">
                              <div class="col-md-6">
                                  <h3>Nom</h3>
                                <div class="form-group">
                                    <p class="form-control">{{$data['RLastname']}}</p>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Prénom</h3>
                                <div class="form-group">
                                  <p class="form-control">{{$data['RFirstname']}}</p>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Numéro de téléphone :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                    <p class="form-control">{{$data['RPhoneNumber']}}</p>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Etat matrimonial</h3>
                                <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="form-control fournisseur-elements">
                                          <p>{{$data['etat_matrimonial']}}</p>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <h1 class="mb-4">Status dossier :</h1>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="form-control fournisseur-elements">
                                        <p>{{$data['status']}}</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Nom - Email de l'installateur</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="form-control">
                                      {{$instas->name ?? ''}} - {{$instas->email ?? ''}}
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Nom - Email de l'assistante sociale</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="form-control">
                                      {{$assist->name ?? ''}} - {{$assist->email ?? ''}}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Status</h3>
                                    <div class="form-group">
                                        <div class="input-group mb-4">
                                          <div class="form-control fournisseur-elements">
                                             {{$data['home_status']}}
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Escalier :</h3>
                                    <div class="form-group">
                                        <div class="input-group mb-4">
                                          <div class="form-control fournisseur-elements">
                                              <p>{{$data['stairs']}}</p>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @hasroles(['admin', 'user', 'Assistante sociale'])
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Mail AL</h3>
                                    <div class="form-group">
                                        <div class="input-group mb-4">
                                            <p class="form-control" id="mail_al">{{$data['mail_al']}}</p>
                                            <div class="input-group-prepend">
                                              <button class="btn btn-primary btn-round clip" type="button" id="button-addon3" data-clipboard-target="#mail_al">Copié</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <h3>Password AL</h3>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group mb-3">
                                        <p class="form-control" id="pwd_al">{{$data['password_al']}}</p>
                                        <div class="input-group-prepend">
                                          <button class="btn btn-primary btn-round clip" type="button" id="button-addon1" data-clipboard-target="#pwd_al">Copié</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            @endhasroles
                            <h1 class="mb-4">Produit(s) :</h1>
                            <table id="readRefTable" class="table table-striped table-bordered w-100 dt-table-false responsive display nowrap">
                              <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Fournisseur</th>
                                    <th class="text-center">Type de référence</th>
                                    <th class="text-center">Type de verre</th>
                                    <th class="text-center">Type de porte</th>
                                    <th>Dimension</th>
                                    <th class="text-center">Référence</th>
                                    <th class="text-center" width="10%">Copier la référence</th>
                                </tr>
                              </thead>
                              <tbody>
                                @if(!empty($ref->id))
                                <tr>
                                    <td class="text-center">{{$ref->id}}</td>
                                    <td class="text-center">{{$ref->fournisseur}}</td>
                                    <td class="text-center">{{$ref->type}}</td>
                                    <td class="text-center">{{$ref->Pparams}}</td>
                                    <td class="text-center">{{$ref->ProduitType}}</td>
                                    <td class="text-center">{{$ref->dimension}}</td>
                                    <td class="text-center" id="reference">{{$ref->ref}}</td>
                                    <td class="text-center" width="5%"><button class="btn btn-primary btn-round clip" type="button" id="button-addon2" data-clipboard-target="#reference">Copié</button></td>
                                </tr>
                                @endif
                              </tbody>
                            </table>
                        <hr class="my-3">
                        <h1 class="mb-4">Devis annexe :</h1>
                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group mb-4">
                                <div class="input-group">
                                  <div class="form-control fournisseur-elements">
                                      <p>{{$data['devis']}}</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-6">
                              <h3>Descriptif :</h3>
                            <div class="form-group">
                              <div class="input-group mb-4">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly="readonly">{{$data['descriptif']}}</textarea>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <h3>Commentaire :</h3>
                            <div class="form-group">
                              <div class="input-group mb-4">
                                <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" readonly="readonly">{{$data['commentaire']}}</textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="alert alert-danger" id="date_alert" role="alert" style="display: none">
                              Votre date est vide
                            </div>
                          </div>
                        </div>
                        <h1 class="mb-4">Dates :</h1>
                        <table id="readDatesTable" class="table table-striped table-bordered w-100 dt-table responsive display nowrap">
                            <thead>
                                <tr>
                                  <th class="text-center">Nom</th>
                                  <th class="text-center">DATE APPEL</th>
                                  <th class="text-center">DATE INTERVENTION</th>
                                  <th class="text-center">DATE ASSISTANTE SOCIALE</th>
                                </tr>
                            </thead>
                            <tbody id="addDateTbody">
                              @foreach ($dates as $date)
                                  <tr>
                                      <td class="text-center">{{$date->name}}</td>
                                      <td class="text-center">{{$date->date_appel}}</td>
                                      <td class="text-center">{{$date->date_intervention}}</td>
                                      <td class="text-center">{{$date->date_assist_social}}</td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <h3>Source :</h3>
                            <p class="form-control">{{$data["source"]}}</p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="input-group mb-4">
                                @if(!glob(storage_path('app/public/prospects/requirements/'.$id.'/Fin_chantier/Fin_de_chantier.*')))
                                  <form action="{{route('addDocs', $id)}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <h3>Document de fin de chantier</h3>
                                    <input type="file" class="form-control-file" id="finChantier" name="requirementsFile" required>
                                    <button type="submit" class="btn btn-primary mt-4">Ajouter</button>
                                  </form>
                                  @else
                                  @foreach (array_diff(scandir(public_path('/storage/prospects/requirements/'.$id.'/Fin_chantier/')), array('..', '.')) as $value)
                                  <h3>Fiche de fin de chantier :</h3>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <embed src="{{asset('/storage/prospects/requirements/'.$id.'/Fin_chantier/'.$value)}}" class="img-thumbnail">
                                      </div>
                                    </div>
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group">
                                        <a href="{{asset('/storage/prospects/requirements/'.$id.'/Fin_chantier/'.$value)}}" download="Fiche_fin_de_chantier_{{$data['pfullname']}}" class="btn btn-info"> 
                                          Télécharger
                                        </a> 
                                        <button type="button" onclick='printJS(`{{asset("storage/prospects/requirements/".$id."/Fin_chantier/".$value)}}`)' class="btn btn-info">Imprimer</button>
                                        <form action="{{route('deleteDocs', ['id' => $id, 'doc' => 'Fin_chantier'])}}" method="post">
                                          {{ csrf_field() }}
                                          @method('delete')
                                          <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </form>
                                       
                                      </div>
                                    </div>
                                  </div>
                                  @endforeach
                                  @endif
                            </div>
                            </div>
                          </div>
                        </div>
                        @hasroles(['admin', 'user', 'Assistante sociale'])
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                            Supprimer la fiche client
                          </button>
                        @endhasroles
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="documentModal" tabindex="-1" aria-labelledby="documentModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="documentModalLabel">Les documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="row">
                    <div class="form-group">
                      <div class="input-group mb-4">
                        @foreach ($documents as $key => $item)
                        <div class="col-md-6">
                          <div class="card">
                            <embed src="{{asset('storage\\'.end($item))}}" class="card-img-top"/>
                            <div class="card-body">
                              <h5 class="card-title">{{$item[0]}}</h5>
                              <a href="{{asset('storage\\'.end($item))}}" download="{{$item[0]}}" class="btn btn-info"> 
                                Télécharger
                              </a> 
                            <button type="button" onclick='printJS(`{{asset("storage/".end($item))}}`)' class="btn btn-info">Imprimer</button>
                            <form action="{{route('deleteDocs', ['id' => $id, 'doc' => $item[0]])}}" method="post">
                              {{ csrf_field() }}
                              @method('delete')
                              <button type="submit" class="btn btn-danger mt-2">Supprimer</button>
                            </form>
                            </div>
                          </div>
                        </div>
                      @endforeach                          
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        
        @hasroles(['admin', 'user', 'Assistante sociale'])
          <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <form action="{{route('destroy', $id)}}" method="POST">
              @csrf
              @method('delete')
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Supprimer la fiche {{$id}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p>Attention, vous êtes sur le point de supprimer la fiche numéro {{$id}} êtes vous sûr ?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          @endhasroles

        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="{{asset('js/clipboard/clipboard.min.js?ver=1.1')}}"></script>
<script src="{{asset('js/print.min.js?ver=1.1')}}"></script>
<script src="{{asset('js/tables.js?ver=1.1')}}"></script>
<script src="{{asset('js/read.js?ver=1.1')}}"></script>
@endsection
