$(document).ready(function(){ 
    var table = $('#list_table').DataTable({
        processing: true,
        serverSide: true,
        fixedHeader: true,
        bInfo: false,
        ajax: {
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/prospect/read',
            type: 'POST',
        },
        dom: 'lBfrtip',
       
        buttons: [{
            extend: 'copy',
            text: 'Copier le tableau',
            key: {
                key: 'c',
                ctrlKey: true
            }
        }, 
        {
            extend: 'pdf',
            exportOptions:{
                columns:':visible',
            },
         }, 
        {
            extend: 'excel',
            exportOptions:{
                columns:':visible',
            },
        },
        {
            extend: 'colvis',
            text: 'Filter le tableau',
        },
        {
            extend: 'print',
            text: 'Imprimer le tableau',
        }],
        columnDefs: [ 
            {
                targets: 0,
                'data': 'id',
                'name': 'id'
            },
            {
                targets: 1, 
                "data": 'full_name',
                "name": "full_name",
            },
            {
                targets: 2, 
                "data": "statusName",
                'name': "statusName"
            },
            {
                targets: 3,
                "data": "home_status",
                "name": "home_status",
            },
            {
                "targets": 4,
                "data": 'personal_phone',
                "name": "personal_phone",
            },
            {
                "targets": 5,
                "data": 'home_phone',
                "name": "home_phone",
            },
            {
                targets: 6,
                "data": "address",
                "name": "address"
            },
            {
                targets: 7,
                "data": "zip_code",
                "name": "zip_code"
            },
            {
                targets: 8,
                "data": "city",
                "name": "city"
            },
            {
                targets: 9,
                "data": "stairs",
                "name": "stairs"
            },
            {
                targets: 10,
                "data": "mail_al",
                "name": "mail_al"
            },
            {
                targets: 11,
                "data": "source",
                "name": "source"
            },
            {
                targets: 12,
                "data": "référence",
                "name": "référence"
            },
            {
                "targets": -1,
                "data": null,
                "defaultContent": `<button class="btn btn-primary">Détails</button>`
            }
            
        ],    
    });
    $('#list_table tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        if(!data){
            data = table.row( $(this).parents('tr').prev() ).data();
        }
        return window.location.replace(`/prospect/read/${data.id}`);
    } );
});