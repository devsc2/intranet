<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = [
            ['name' => 'Admin'],
            ['name' => 'User'],
            ['name' => 'Installateur'],
            ['name' => 'Assistante sociale']
        ];

        Role::insertOrIgnore($roles);
    }
}
