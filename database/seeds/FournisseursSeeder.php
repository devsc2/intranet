<?php

use Illuminate\Database\Seeder;

class FournisseursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fournisseurs')->insert([
            ['name' => 'Kindeo'],
            ['name' => 'Autre']
        ]);

        DB::table('fournisseurs_types')->insertOrIgnore(
            array(
                ["id_fournisseur" => 1, "name" => "Mécanique - en niche"],
                ["id_fournisseur" => 1, "name" => "Mécanique - en angle"],
                ["id_fournisseur" => 1, "name" => "Thermostatique - en niche"],
                ["id_fournisseur" => 1, "name" => "Thermostatique - en angle"]
            )
        );

        DB::table('fournisseurs_version')->insertOrIgnore([
            ["id_fournisseur" => 1, "name" => "Haute"],
            ["id_fournisseur" => 1, "name" => "Mixte"],
            ["id_fournisseur" => 1, "name" => "Basse"]
        ]);

        DB::table('fournisseurs_produits_types')->insertOrIgnore([
            ["id_fournisseur" => 1, "name" => "Porte coulissante"],
            ["id_fournisseur" => 1, "name" => "Porte pivotante"],
            ["id_fournisseur" => 1, "name" => "Espace ouvert"],
        ]);

        DB::table('fournisseurs_produits_types_params')->insertOrignore([
            ["id_fournisseur" => 1, "name" => "Verre transparent"],
            ["id_fournisseur" => 1, "name" => "Verre bande centrale dépolie"],
        ]);
 // Kindeo - Thermostatique en angle - mixte - porte coulissante - Verre transparent
        DB::table('fournisseurs_produits_dimensions')->insertOrIgnore([
            ["name" => "100/135 x 69/86"],
            ["name" => "100/135 x 79/96"],
            ["name" => "100/135 x 89/106"],
            ["name" => "120/155 x 69/86"],
            ["name" => "120/155 x 79/96"],
            ["name" => "120/155 x 89/106"],
            ["name" => "140/175 x 69/86"],
            ["name" => "140/175 x 79/96"],
            ["name" => "140/175 x 89/106"],
            ["name" => "160/195 x 69/86"],
            ["name" => "160/195 x 79/96"],
            ["name" => "160/195 x 89/106"],
            ["name" => "170/205 x 69/86"],
            ["name" => "170/205 x 79/96"],
            ["name" => "170/205 x 89/106"],
        ]);

        DB::table('fournisseurs_produits')->insertOrIgnore(
            [
                // Kindeo - Mécanique en niche - version haute - porte coulissante - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHC-TN2-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHC-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHC-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHC-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHC-TN2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHC-TN2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHC-TN2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHC-TN2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHC-TN2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHC-TN2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHC-TN2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHC-TN2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHC-TN2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHC-TN2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHC-TN2-GST"
                ],
                // Kindeo - Mécanique en niche - version haute - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHC-DB2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHC-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHC-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHC-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHC-DB2-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHC-DB2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHC-DB2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHC-DB2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHC-DB2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHC-DB2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHC-DB2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHC-DB2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHC-DB2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHC-DB2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHC-DB2-GS2"
                ],
                // kindeo - mécanique en niche - haute - porte pivotante - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHP-TN2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHP-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHP-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHP-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHP-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHP-TN2-GSQ"
                ],
                // kindeo - mécanique en niche - haute - espace ouvert - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHS-TN2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHS-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHS-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHS-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHS-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHS-TN2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHS-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHS-TN2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHS-TN2-GSD"
                ],
                // kindeo - mécanique en niche - haute - porte pivotante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHP-DB2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHP-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHP-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHP-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHP-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHP-DB2-GSZ"
                ],
                // kindeo - mécanique en niche - haute - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHS-DB2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHS-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHS-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHS-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHS-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHS-DB2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHS-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHS-DB2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHS-DB2-GSM"
                ],
                // kindeo - mécanique en angle - haute - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHC-TN2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHC-TN2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHC-TN2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHC-TN2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHC-TN2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHC-TN2-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHC-TN2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHC-TN2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHC-TN2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHC-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHC-TN2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHC-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHC-TN2-GS6"
                ],
                // kindeo - mécanique en angle - haute - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHC-DB2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHC-DB2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHC-DB2-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHC-DB2-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHC-DB2-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHC-DB2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHC-DB2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHC-DB2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHC-DB2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHC-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHC-DB2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHC-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHC-DB2-GSF"
                ],
                // Kindeo - mécanique en angle - haute - porte pivotante - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHP-TN2-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHP-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHP-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHP-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHP-TN2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHP-TN2-GS3"
                ],
                // Kindeo - mécanique en angle - haute - espace ouvert - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHS-TN2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHS-TN2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHS-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHS-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHS-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHS-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHS-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHS-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHS-TN2-GSQ"
                ],
                // Kindeo - mécanique en angle - haute - porte pivotante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHP-DB2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHP-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHP-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHP-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHP-DB2-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHP-DB2-GSC"
                ],
                // Kindeo - mécanique en angle - haute - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHS-DB2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHS-DB2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHS-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHS-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHS-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHS-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13,
                    "référence" => "K7-1707-AHS-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHS-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHS-DB2-GSZ"
                ],
                // Kindeo - Thermostatique en niche - haute - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHC-TN3-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHC-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHC-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHC-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHC-TN3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHC-TN3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHC-TN3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHC-TN3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHC-TN3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHC-TN3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHC-TN3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHC-TN3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHC-TN3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHC-TN3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHC-TN3-GSR"
                ],
                // Kindeo - Thermostatique en niche - haute - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHC-DB3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHC-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHC-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHC-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHC-DB3-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHC-DB3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHC-DB3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHC-DB3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHC-DB3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHC-DB3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHC-DB3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHC-DB3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHC-DB3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHC-DB3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHC-DB3-GS0"
                ],
                // Kindeo - Thermostatique en niche - haute - porte pivotantes - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHP-TN3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHP-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHP-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHP-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHP-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHP-TN3-GSO"
                ],
                // Kindeo - Thermostatique en niche - haute - espace ouvert - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHS-TN3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHS-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHS-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHS-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHS-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHS-TN3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHS-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHS-TN3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHS-TN3-GSB"
                ],
                // Kindeo - Thermostatique en niche - haute - porte pivotante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NHP-DB3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NHP-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NHP-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NHP-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NHP-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NHP-DB3-GSX"
                ],
                // Kindeo - Thermostatique en niche - haute - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NHS-DB3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NHS-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NHS-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NHS-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NHS-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NHS-DB3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NHS-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NHS-DB3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NHS-DB3-GSK"
                ],
                // Kindeo - Thermostatique en angle - haute - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHC-TN3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHC-TN3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHC-TN3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHC-TN3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHC-TN3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHC-TN3-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHC-TN3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHC-TN3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHC-TN3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHC-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHC-TN3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHC-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHC-TN3-GS4"
                ],
                // Kindeo - Thermostatique en angle - haute - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHC-DB3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHC-DB3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHC-DB3-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHC-DB3-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHC-DB3-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHC-DB3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHC-DB3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHC-DB3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHC-DB3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHC-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHC-DB3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHC-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHC-DB3-GSD"
                ],
                // Kindeo - Thermostatique en angle - haute - porte pivotante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHP-TN3-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHP-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHP-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHP-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHP-TN3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHP-TN3-GS1"
                ],
                // Kindeo - Thermostatique en angle - haute - espace ouvert - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHS-TN3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHS-TN3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHS-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHS-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHS-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHS-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHS-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHS-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHS-TN3-GSO"
                ],
                // Kindeo - Thermostatique en angle - haute - porte pivotante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AHP-DB3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AHP-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AHP-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AHP-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AHP-DB3-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AHP-DB3-GSA"
                ],
                // Kindeo - Thermostatique en niche - haute - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AHS-DB3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AHS-DB3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AHS-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AHS-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AHS-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AHS-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AHS-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AHS-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 1, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AHS-DB3-GSX"
                ],
                // Kindeo - Mécanique en niche - mixte - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMC-TN2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMC-TN2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMC-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMC-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMC-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMC-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMC-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMC-TN2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMC-TN2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMC-TN2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMC-TN2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMC-TN2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMC-TN2-GSI"
                ],
                // Kindeo - Mécanique en niche - mixte - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMC-DB2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMC-DB2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMC-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMC-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMC-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMC-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMC-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMC-DB2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMC-DB2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMC-DB2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMC-DB2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMC-DB2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMC-DB2-GSR"
                ],
                 // Kindeo - Mécanique en niche - mixte - porte pivotante - Verre transparent
                 [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMP-TN2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMP-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMP-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMP-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMP-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMP-TN2-GSF"
                ],
                 // Kindeo - Mécanique en niche - mixte - espace ouvert - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMS-TN2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMS-TN2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMS-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMS-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMS-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMS-TN2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMS-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMS-TN2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMS-TN2-GS2"
                ],
                 // Kindeo - Mécanique en niche - mixte - porte pivotante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMP-DB2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMP-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMP-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMP-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMP-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMP-DB2-GSO"
                ],
                 // Kindeo - Mécanique en niche - mixte - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMS-DB2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMS-DB2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMS-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMS-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMS-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMS-DB2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMS-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMS-DB2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMS-DB2-GSB"
                ],
                // Kindeo - Mécanique en angle - mixte - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMC-TN2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMC-TN2-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMC-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMC-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMC-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMC-TN2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMC-TN2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMC-TN2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMC-TN2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMC-TN2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMC-TN2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMC-TN2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMC-TN2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMC-TN2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMC-TN2-GSV"
                ],
                // Kindeo - Mécanique en angle - mixte - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMC-DB2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMC-DB2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMC-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMC-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMC-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMC-DB2-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMC-DB2-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMC-DB2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMC-DB2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMC-DB2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMC-DB2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMC-DB2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMC-DB2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMC-DB2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMC-DB2-GS4"
                ],
                // Kindeo - Mécanique en angle - mixte - porte pivotantes - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMP-TN2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMP-TN2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMP-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMP-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMP-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMP-TN2-GSS"
                ],
                // Kindeo - Mécanique en angle - mixte - espace ouvert - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMS-TN2-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMS-TN2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMS-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMS-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMS-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMS-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMS-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMS-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMS-TN2-GSF"
                ],
                // Kindeo - Mécanique en angle - mixte - porte pivotantes - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMP-DB2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMP-DB2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMP-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMP-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMP-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMP-DB2-GS1"
                ],
                // Kindeo - Mécanique en angle - mixte - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMS-DB2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMS-DB2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMS-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMS-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMS-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMS-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMS-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMS-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMS-DB2-GSO"
                ],
                // Kindeo - Thermostatique en niche - mixte - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMC-TN3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMC-TN3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMC-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMC-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMC-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMC-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMC-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMC-TN3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMC-TN3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMC-TN3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMC-TN3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMC-TN3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMC-TN3-GSG"
                ],
                // Kindeo - Thermostatique en niche - mixte - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMC-DB3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMC-DB3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMC-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMC-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMC-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMC-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMC-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMC-DB3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMC-DB3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMC-DB3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMC-DB3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMC-DB3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMC-DB3-GSP"
                ],
                // Kindeo - Thermostatique en niche - mixte - porte pivotante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMP-TN3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMP-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMP-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMP-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMP-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMP-TN3-GSD"
                ],
                 // Kindeo - Thermostatique en niche - mixte - espace ouvert - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMS-TN3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMS-TN3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMS-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMS-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMS-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMS-TN3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMS-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMS-TN3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMS-TN3-GS0"
                ],
                // Kindeo - Thermostatique en niche - mixte - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NMP-DB3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NMP-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NMP-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NMP-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NMP-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NMP-DB3-GSM"
                ],
                // Kindeo - Thérmostatique en niche - mixte - espace ouvert - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NMS-DB3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NMS-DB3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NMS-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NMS-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NMS-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NMS-DB3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NMS-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NMS-DB3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NMS-DB3-GS9"
                ],
                // Kindeo - Thermostatique en angle - mixte - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMC-TN3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1709-NMS-DB3-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMC-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMC-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMC-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMC-TN3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMC-TN3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMC-TN3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMC-TN3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMC-TN3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMC-TN3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMC-TN3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMC-TN3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMC-TN3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMC-TN3-GST"
                ],
                // Kindeo - Thermostatique en angle - mixte - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMC-DB3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMC-DB3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMC-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMC-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMC-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMC-DB3-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMC-DB3-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMC-DB3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMC-DB3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMC-DB3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMC-DB3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMC-DB3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMC-DB3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMC-DB3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMC-DB3-GS2"
                ],
                // Kindeo - Thermostatique en angle - mixte - porte PIVOTANTE - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMP-TN3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMP-TN3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMP-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMP-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMP-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMP-TN3-GSQ"
                ],
                // Kindeo - Thermostatique en angle - mixte - ESPACE OUVERT - Verre transparenT
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMS-TN3-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMS-TN3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMS-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMS-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMS-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMS-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMS-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMS-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMS-TN3-GSD"
                ],
                // Kindeo - Thermostatique en angle - mixte - porte PIVOTANTE - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-AMP-DB3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-AMP-DB3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-AMP-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-AMP-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-AMP-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-AMP-DB3-GSZ"
                ],
                // Kindeo - Thermostatique en angle - mixte - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-AMS-DB3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-AMS-DB3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-AMS-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-AMS-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-AMS-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-AMS-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-AMS-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-AMS-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 2, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-AMS-DB3-GSM"
                ],
                // Kindeo - Mécanique en niche - Basse - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBC-TN2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBC-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBC-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBC-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBC-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBC-TN2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBC-TN2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBC-TN2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBC-TN2-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBC-TN2-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBC-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBC-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBC-TN2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBC-TN2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBC-TN2-GS5"
                ],
                // Kindeo - Mécanique en niche - BASSE - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBC-DB2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBC-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBC-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBC-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBC-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBC-DB2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBC-DB2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBC-DB2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBC-DB2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBC-DB2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBC-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBC-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBC-DB2-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBC-DB2-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBC-DB2-GSE"
                ],
                // Kindeo - Mécanique en niche - version BASSE - porte PIVOTANTE - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBP-TN2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBP-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBP-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBP-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBP-TN2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBP-TN2-GS2"
                ],
                // Kindeo - Mécanique en niche - version BASSE - ESPACE OUVERT - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBS-TN2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBS-TN2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBS-TN2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBS-TN2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBS-TN2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBS-TN2-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBS-TN2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBS-TN2-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBS-TN2-GSP"
                ],
                // Kindeo - Mécanique en niche - version BASSE - porte PIVOTANTE - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBP-DB2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBP-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBP-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBP-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBP-DB2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBP-DB2-GSB"
                ],
                // Kindeo - Mécanique en niche - version BASSE - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBS-DB2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBS-DB2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBS-DB2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBS-DB2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBS-DB2-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBS-DB2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBS-DB2-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBS-DB2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 1, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBS-DB2-GSY"
                ],
                // Kindeo - Mécanique en ANGLE - version BASSE - porte coulissante - verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABC-TN2-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABC-TN2-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABC-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABC-TN2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABC-TN2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABC-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABC-TN2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABC-TN2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABC-TN2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABC-TN2-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABC-TN2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABC-TN2-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABC-TN2-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABC-TN2-GSI"
                ],
                // Kindeo - Mécanique en ANGLE - version BASSE - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABC-DB2-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABC-DB2-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABC-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABC-DB2-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABC-DB2-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABC-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABC-DB2-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABC-DB2-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABC-DB2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABC-DB2-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABC-DB2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABC-DB2-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABC-DB2-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABC-DB2-GSR"
                ],
                // Kindeo - MECANIQUE en angle - BASSE - porte PIVOTANTE - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABP-TN2-GSN"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABP-TN2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABP-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABP-TN2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABP-TN2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABP-TN2-GSF"
                ],
                // Kindeo - MECANIQUE en angle - BASSE - ESPACE OUVERT - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABS-TN2-GSC"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABS-TN2-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABS-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABS-TN2-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABS-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABS-TN2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABS-TN2-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABS-TN2-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABS-TN2-GS2"
                ],
                // Kindeo - MECANIQUE en angle - BASSE - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABP-DB2-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABP-DB2-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABP-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABP-DB2-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABP-DB2-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABP-DB2-GSO"
                ],
                // Kindeo - MECANIQUE en angle - BASSE - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABS-DB2-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABS-DB2-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABS-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABS-DB2-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABS-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABS-DB2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABS-DB2-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABS-DB2-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 2, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABS-DB2-GSB"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBC-TN3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBC-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBC-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBC-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBC-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBC-TN3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBC-TN3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBC-TN3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBC-TN3-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBC-TN3-GS9"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBC-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBC-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBC-TN3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBC-TN3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBC-TN3-GS3"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBC-DB3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBC-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBC-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBC-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBC-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBC-DB3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBC-DB3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBC-DB3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBC-DB3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBC-DB3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBC-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBC-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBC-DB3-GSG"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBC-DB3-GSE"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBC-DB3-GSC"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - porte PIVOTANTE - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBP-TN3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBP-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBP-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBP-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBP-TN3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBP-TN3-GS0"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - ESPACE OUVERT - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBS-TN3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBS-TN3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBS-TN3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBS-TN3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBS-TN3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBS-TN3-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBS-TN3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBS-TN3-GSP"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBS-TN3-GSN"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - porte PIVOTANTE - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-NBP-DB3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-NBP-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-NBP-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-NBP-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-NBP-DB3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-NBP-DB3-GS9"
                ],
                // Kindeo - Thermostatique en NICHE - BASSE - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-NBS-DB3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-NBS-DB3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-NBS-DB3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-NBS-DB3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-NBS-DB3-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-NBS-DB3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-NBS-DB3-GS0"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-NBS-DB3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 3, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-NBS-DB3-GSW"
                ],
                // Kindeo - Thermostatique en angle - BASSE - porte coulissante - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABC-TN3-GSY"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABC-TN3-GSW"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABC-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABC-TN3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABC-TN3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABC-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABC-TN3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABC-TN3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABC-TN3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABC-TN3-GSM"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABC-TN3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABC-TN3-GSK"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABC-TN3-GSI"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABC-TN3-GSG"
                ],
                // Kindeo - Thermostatique en angle - BASSE - porte coulissante - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABC-DB3-GS7"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABC-DB3-GS5"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABC-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABC-DB3-GS3"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABC-DB3-GS1"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABC-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABC-DB3-GSZ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABC-DB3-GSX"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABC-DB3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABC-DB3-GSV"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABC-DB3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABC-DB3-GST"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABC-DB3-GSR"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 1, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABC-DB3-GSP"
                ],
                // Kindeo - Thermostatique en angle - BASSE - porte PIVOTANTE - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABP-TN3-GSL"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABP-TN3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABP-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABP-TN3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABP-TN3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABP-TN3-GSD"
                ],
                // Kindeo - Thermostatique en angle - BASSE - ESPACE OUVERT - Verre transparent
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABS-TN3-GSA"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABS-TN3-GS8"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABS-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABS-TN3-GS6"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABS-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABS-TN3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABS-TN3-GS4"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABS-TN3-GS2"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 1, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABS-TN3-GS0"
                ],
                // Kindeo - Thermostatique en angle - BASSE - porte PIVOTANTE - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 1, 
                    "référence" => "K7-1007-ABP-DB3-GSU"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 2, 
                    "référence" => "K7-1008-ABP-DB3-GSS"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 3, 
                    "référence" => "K7-1009-ABP-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 4, 
                    "référence" => "K7-1207-ABP-DB3-GSQ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 5, 
                    "référence" => "K7-1208-ABP-DB3-GSO"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 2, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 6, 
                    "référence" => "K7-1209-ABP-DB3-GSM"
                ],
                // Kindeo - Thermostatique en angle - BASSE - ESPACE OUVERT - Verre bande centrale dépolie
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 7, 
                    "référence" => "K7-1407-ABS-DB3-GSJ"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 8, 
                    "référence" => "K7-1408-ABS-DB3-GSH"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 9, 
                    "référence" => "K7-1409-ABS-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 10, 
                    "référence" => "K7-1607-ABS-DB3-GSF"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 11, 
                    "référence" => "K7-1608-ABS-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 12, 
                    "référence" => "K7-1609-ABS-DB3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 13, 
                    "référence" => "K7-1707-ABS-DB3-GSD"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 14, 
                    "référence" => "K7-1708-ABS-DB3-GSB"
                ],
                [
                    "id_fournisseur" => 1, 
                    "id_type" => 4, 
                    "id_version" => 3, 
                    'id_produit_type' => 3, 
                    "id_produit_type_param" => 2, 
                    'id_dimension' => 15, 
                    "référence" => "K7-1709-ABS-DB3-GS9"
                ],

            ]
                
        );

    }
}
