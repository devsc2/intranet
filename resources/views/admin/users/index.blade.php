@extends('layouts.app')
@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Ici vous pouvez gérer tous les utilisateurs. Bien sûr, seulement les administrateurs ont accés à cette page. Vous pouvez modifier, supprimer tout le monde mis appart vous, bien entendu.'),
        'img' => 'user-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                        <div class="align-items-center">
                            @if (\Session::has('success'))
                                <div class="alert alert-success" role="alert">
                                    {!! \Session::get('success') !!}
                                </div>
                            @elseif(\Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    {!! \Session::get('error') !!}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    <h1>La liste des utilisateurs</h1>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <button class="btn btn-info text-right" data-toggle="modal" data-target="#addUserModal">Ajouter un utilisateur</button>
                                    <div class="modal fade text-left" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.users.store')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="addUserModalLabel">Ajouter un utilisateur</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                      <div class="row">
                                                          <div class="col-lg-6">
                                                              <div class="form-group">
                                                                  <label for="userAddName" class="form-control-label">Nom :</label>
                                                                  <input type="text" name="name" id="userAddName" class="form-control" required>
                                                              </div>
                                                          </div>
                                                          <div class="col-lg-6">
                                                              <div class="form-group">
                                                                  <label for="userAddEmail" class="form-control-label">Email :</label>
                                                                  <input type="email" name="email" id="userAddEmail" class="form-control" required>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label for="userAddPassword" class="form-control-label">Mot de passe :</label>
                                                                <input type="password" name="password" id="userAddPassword" class="form-control" required>
                                                            </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col">
                                                            <p class="form-control-label">Roles :</p>
                                                            @foreach ($roles as $role)
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="userAddRole{{$role->id}}" name="userRoles" value="{{$role->id}}" class="custom-control-input" {{$role->name == 'User'?'checked':''}}>
                                                                    <label class="custom-control-label" for="userAddRole{{$role->id}}">{{$role->name}}</label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                      </div>
                                                      
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Créer l'utilisateur</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </form>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="usersTable" class="table w-100 dt-table responsive display nowrap">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="id">#</th>
                                    <th scope="col" class="sort" data-sort="Name">Nom</th>
                                    <th scope="col" class="sort" data-sort="Email">Email</th>
                                    <th scope="col" class="sort" data-sort="role">Role(s)</th>
                                    <th scope="col" class="sort" data-sort="actions">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                
                                @foreach ($users as $user)
                                    <tr>
                                        <th>{{$user->id}}</th>
                                        <th>{{$user->name}}</th>
                                        <th>{{$user->email}}</th>
                                        <th>{{implode(', ', $user->roles()->get()->pluck('name')->toArray())}}</th>
                                        <th>
                                            @if ($user->id !== auth()->user()->id)
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user{{$user->id}}EditModal"><i class="fas fa-user-edit"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#user{{$user->id}}DeleteModal"><i class="fas fa-user-times"></i></button>
                                            @else
                                                <a href="{{route('profile.edit')}}">
                                                    <button type="button" class="btn btn-primary"><i class="fas fa-user-edit"></i></button>
                                                </a>
                                            @endif
                                        </th>
                                    </tr>
                                    <div class="modal fade" id="user{{$user->id}}EditModal" tabindex="-1" role="dialog" aria-labelledby="user{{$user->id}}EditModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.users.update', $user->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('put')
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="user{{$user->id}}EditModalLabel">Modifier {{$user->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="user{{$user->id}}Name" class="form-control-label">Nom :</label>
                                                                <input type="text" class="form-control" id="user{{$user->id}}Name" value="{{$user->name}}" name='name'>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="user{{$user->id}}Email" class="form-control-label">Email :</label>
                                                                <input type="text" class="form-control" id="user{{$user->id}}Email" value="{{$user->email}}" name='email'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <p class="form-control-label">Roles :</p>
                                                                @foreach ($roles as $role)
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input type="radio" id="user{{$user->id}}hasrole{{$role->id}}" name="userRoles" value="{{$role->id}}" class="custom-control-input" {{$user->hasAnyRole($role->name)?'checked':''}}>
                                                                        <label class="custom-control-label" for="user{{$user->id}}hasrole{{$role->id}}">{{$role->name}}</label>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                  </div>
                                                </div>
                                              </div>
                                        </form>
                                        
                                    </div>
                                    <div class="modal fade" id="user{{$user->id}}DeleteModal" tabindex="-1" role="dialog" aria-labelledby="user{{$user->id}}DeleteModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.users.destroy', $user->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('delete')
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="user{{$user->id}}DeleteModalLabel">Supprimer {{$user->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p>Attention, vous allez supprimer définitivement {{$user->name}}, êtes vous sûr ?</p>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-danger">Supprimer</button>
                                                  </div>
                                                </div>
                                              </div>
                                        </form>
                                        
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="{{asset('js/tables.js?ver=1.1')}}"></script>
@endsection
