<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccessMultipleRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);
        if(Auth::user()->hasAnyRoles($roles)){
            return $next($request);
        }
        return abort(404);
    }
}
