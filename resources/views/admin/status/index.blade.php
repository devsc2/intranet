@extends('layouts.app')
@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Ici vous pouvez gérer tous les status. Bien sûr, seulement les administrateurs ont accés à cette page. Vous pouvez modifier, supprimer, créer et même modifier l\'ordre des status !'),
        'img' => 'folder-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                        <div class="align-items-center">
                            @if (\Session::has('success'))
                                <div class="alert alert-success" role="alert">
                                    {!! \Session::get('success') !!}
                                </div>
                            @elseif(\Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    {!! \Session::get('error') !!}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    <h1>La liste des status</h1>
                                    <p class="text-danger">ATTENTION ! Supprimer un statut, supprimera toutes les fiches qui ont le statut supprimé !</p>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#addUserModal">Ajouter un status</button>
                                    <div class="modal fade text-left" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.status.store')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="addUserModalLabel">Ajouter un status</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <input type="text" name="name" id="statuName" placeholder="Nom du status" class="form-control"  onkeyup="this.value = this.value.toUpperCase();">
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Créer le status</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="card-body">
                        <table class="table table-responsive" id='table'>
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-left">#</th>
                                    <th class="text-left" width="100%">Status</th>
                                    <th class="text-right" width="15%">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($status as $statu)
                                    <tr>
                                        <input class='handle text-center' type="hidden" value='{{$statu->id}}'>
                                        <th class='handle text-left'>{{$statu->order}}</th>
                                        <th class='handle text-left'>{{$statu->name}}</th>
                                        <th class="text-right">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#statu{{$statu->id}}EditModal">
                                                <i class="fas fa-user-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#statu{{$statu->id}}DeleteModal">
                                                <i class="fas fa-user-times"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    {{-- Edit modal --}}
                                    <div class="modal fade text-left" id="statu{{$statu->id}}EditModal" tabindex="-1" role="dialog" aria-labelledby="statu{{$statu->id}}EditModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.status.update', $statu->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('put')
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="statu{{$statu->id}}EditModalLabel">Modifier le status {{$statu->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                        <label for="statu{{$statu->id}}Name" class="form-control-label">Nom :</label>
                                                        <input type="text" name="name" id="statu{{$statu->id}}Name" value="{{$statu->name}}" class="form-control"  onkeyup="this.value = this.value.toUpperCase();">
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                                  </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- Delete modal --}}
                                    <div class="modal fade text-left" id="statu{{$statu->id}}DeleteModal" tabindex="-1" role="dialog" aria-labelledby="statu{{$statu->id}}DeleteModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.status.destroy', $statu->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('delete')
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="statu{{$statu->id}}DeleteModalLabel">Supprimer définitivement le status {{$statu->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p class="text-danger">Attention ! Vous êtes sur le point de supprimer définitvement le status {{$statu->name}}, êtes vous sûr ?</p>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-danger">Supprimer</button>
                                                  </div>
                                                </div>
                                              </div>
                                        </form>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="{{asset('js/table-dragger.min.js?ver=1.1')}}"></script>
<script src="{{asset('js/status.js?ver=1.1')}}"></script>
@endsection
