<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_dates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_prospect')->unsigned()->index();
            $table->string('name');
            $table->string('date_appel', 10)->nullable();
            $table->string('date_intervention', 10)->nullable();
            $table->string('date_assist_social', 10)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->unique(['id_prospect', 'name', 'date_appel', 'date_intervention', 'date_assist_social'], 'unique_entries');

        });

        Schema::table('prospect_dates', function (Blueprint $table){
            $table->foreign('id_prospect')->references('id')->on('prospects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_dates');
    }
}
