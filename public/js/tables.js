var tables = {};
$('.dt-table').each(function(){
    tables[$(this).attr('id')] = $(this).DataTable({
        responsive: true,
        bInfo: false,
        bLengthChange: false,
    })
})
$('.dt-table-false').each(function(){
    tables[$(this).attr('id')] = $(this).DataTable({
        responsive: true,
        searching: false,
        bLengthChange: false,
        bInfo: false,
        bPaginate: false,
    })
})
