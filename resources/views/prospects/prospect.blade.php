@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('js/Semantic/semantic.min.css')}}">
@endsection
@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Bienvenue sur la page de création d\'une fiche client !'),
        'img' => 'prospect-create-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                      <p id="test"></p>
                        <div class="col alert alert-info" id="ficheInfo" role="alert" style="display: none">
                          Votre fiche client est en cours de création ! Redirection dans quelques secondes...
                        </div>
                        <div class="align-items-center">
                            <h1 class="col-12 mb-0 text-center">Créer une fiche client</h1>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action='/' method='GET' id="myForm" enctype="multipart/form-data">
                            <h1 class="mb-4">Client :</h1>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="sexe" id="sexeMonsieur" value="Monsieur" class="custom-control-input" required>
                                    <label class="custom-control-label" for="sexeMonsieur">Monsieur</label>
                                  </div>
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="sexe" id="sexeMadame" value="Madame" class="custom-control-input" required>
                                    <label class="custom-control-label" for="sexeMadame">Madame</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Nom complet" name="full_name" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <input placeholder="Adresse"class="form-control" name="address"/>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="number" name="zip_code" class="form-control" placeholder="Code postal" maxlength="5">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="text" name="city" placeholder="Ville" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                                    </div>
                                    <input type="tel" class="form-control" placeholder="Tél fixe" name="fixePhoneNumber"/>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <input type="tel" class="form-control" placeholder="Tél portable" name="phoneNumber">
                                    <div class="input-group-append">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr class="my-3">
                            <h1 class="mb-4">Référent :</h1>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Nom" name="referentLastName">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Prénom" name="referentFirstName"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                    <input type="tel" class="form-control" placeholder="Téléphone" name="referentPhoneNumber"/>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="ni ni-satisfied"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Etat matrimonial" name="referentStatus"/>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr class="my-3">
                            <h1 class="mb-4">Status dossier :</h1>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <select class="form-control" id="exampleFormControlSelect1" name="folderStatus" style='cursor: pointer;'>
                                      @foreach ($status as $key => $item)
                                          <option value="{{$key}}">{{$item}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="installateurs" class="form-control-label">Installateur :</label>
                                  <div class="input-group mb-4">
                                    <select class="form-control" id="installateurs" name="users[]" required="" style='cursor: pointer;'>
                                      @foreach ($installateurs as $key => $item)
                                          <option value="{{$item->id}}">{{$item->name}} - {{$item->email}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="installateurs" class="form-control-label">Assistance sociale :</label>
                                  <div class="input-group mb-4">
                                    <select class="form-control" id="assist" name="users[]" style='cursor: pointer;'>
                                      <option value="">Aucun</option>
                                      @foreach ($assists as $key => $item)
                                          <option value="{{$item->id}}">{{$item->name}} - {{$item->email}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @foreach ($home_status as $item)
                              <div class="custom-control custom-radio mb-3 text-uppercase">
                              <input name="status" class="custom-control-input" id="home_status_radio{{$item->id}}" type="radio" value="{{$item->id}}" required onclick="requirements({{$item->id}})" data-toggle="modal" data-target="#exampleModal">
                                <label class="custom-control-label" for="home_status_radio{{$item->id}}">{{$item->name}}</label>
                              </div>
                            @endforeach
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Les documents nécessaires</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    {{-- Javascript here --}}
                                    <div id="requirementsInputs">
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                              <h3 class="mb-4">Escalier ?</h3>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="stairsRadio1" name="stairs" class="custom-control-input" value="Oui">
                                <label class="custom-control-label" for="stairsRadio1">Oui</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="stairsRadio2" name="stairs" class="custom-control-input" value="Non" checked>
                                <label class="custom-control-label" for="stairsRadio2">Non</label>
                              </div>
                              <hr class="my-3">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                      </div>
                                      <input type="email" class="form-control" autocomplete="new-password" placeholder="Mail AL" name="mailAL"/>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <input type="password" class="form-control" autocomplete="new-password" placeholder="Mot de passe AL" name="passwordAL">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <h1 class="mb-4">Fournisseur :</h1>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <select class="form-control fournisseur-elements" id="id_fournisseur" required="" style='cursor: pointer;'>
                                        @foreach ($fournisseurs as $key => $fournisseur)
                                            <option value="{{$key}}">{{$fournisseur}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              
                              <div class="row" id="kindeoParams">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="input-group mb-4">
                                        <select class="form-control fournisseur-elements" id="id_type" required="" style='cursor: pointer;'>
                                          @foreach ($fournisseurs_types as $key => $fournisseur_type)
                                              <option value="{{$key}}">{{$fournisseur_type}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="input-group mb-4">
                                        <select class="form-control fournisseur-elements" id="id_version" required="" style='cursor: pointer;'>
                                          @foreach ($fournisseurs_versions as $key => $fournisseur_version)
                                              <option value="{{$key}}">{{$fournisseur_version}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="input-group mb-4">
                                        <select class="form-control fournisseur-elements" id="id_produit_type" required="" style='cursor: pointer;'>
                                          @foreach ($fournisseurs_produits_types as $key => $fournisseur_produit_type)
                                              <option value="{{$key}}">{{$fournisseur_produit_type}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="input-group mb-4">
                                        <select class="form-control fournisseur-elements" id="id_produit_type_param" style='cursor: pointer;'>
                                          @foreach ($fournisseurs_produits_types_params as $key => $fournisseur_produit_type_param)
                                              <option value="{{$key}}">{{$fournisseur_produit_type_param}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="input-group mb-4">
                                        <select class="form-control fournisseur-elements" id="id_dimension" style='cursor: pointer;'>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <div class="row" id="decomposeParams" style="display: none">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="decomposeFournisseur" class="form-control-label">Votre référence</label>

                                    <div class="input-group mb-4">
                                      <input type="text" name="decomposeFournisseur" class="form-control" id="decomposeFournisseur">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="table-responsive overflow-hidden">
                                <table id="createRefTable" class="table table-striped table-bordered w-100 dt-table-false responsive display nowrap">
                                    <thead>
                                      <tr>
                                          <th class="text-center">#</th>
                                          <th class="text-center">Référence</th>
                                      </tr>
                                    </thead>
                                    <tbody id="getReferences"></tbody>
                                </table>
                              </div>
                              <hr class="my-3">
                              <h3 class="mb-4">Devis annexe ?</h3>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="devisRadio1" name="devisAnnexe" class="custom-control-input" value="Oui">
                                <label class="custom-control-label" for="devisRadio1">Oui</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="devisRadio2" name="devisAnnexe" class="custom-control-input" value="Non" checked>
                                <label class="custom-control-label" for="devisRadio2">Non</label>
                              </div>
                              <div class="row mt-4">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descriptif ..." name="descriptif"></textarea>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" placeholder="Commentaire ..." name="commentary"></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <hr class="my-3">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="alert alert-danger" id="date_alert" role="alert" style="display: none">
                                    Votre date est vide
                                  </div>
                                </div>
                              </div>
                              <h1 class="mb-4">Dates :</h1>
                              <table id="createdateTable" class="table table-striped table-bordered dt-table-false w-100">
                                  <thead>
                                      <tr>
                                          <th class="text-center">Nom</th>
                                          <th class="text-center">DATE_APPEL</th>
                                          <th class="text-center">DATE_INTERVENTION</th>
                                          <th class="text-center">DATE_ASSISTANTE_SOCIALE</th>
                                          <th>Actions</th>
                                      </tr>
                                  </thead>
                                  <tbody></tbody>
                              </table>
                              <div class="row">
                                <div class="col-md-6">
                                  <label for="dateName" class="form-control-label">Nom date</label>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                      </div>
                                      <input type="text" class="form-control" name="dateName" id="dateName"/>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <label for="startDate" class="form-control-label">Date appel</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <input class="form-control" placeholder="Start date" id="startDate" name="startDate" type="date"  pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                
                                <div class="col-md-6">
                                  <label for="endDate" class="form-control-label">Date intervention</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                         <input class="form-control" placeholder="End date" id="endDate" name="endDate" type="date" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"> 
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <label for="endDate" class="form-control-label">Date assistance sociale</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                         <input class="form-control" id="assistDate" name="assistDate" type="date" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"> 
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <button type='button' class="btn btn-primary btn-round d-flex justify-content-between align-items-center" onclick="addDate('createdateTable')"><i class="ni ni-cloud-upload-96 mr-2"></i> 
                                    Ajouter une date
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="ui sub header">Source</div>
                                      <div class="ui fluid search selection dropdown" id='dropdown'>
                                          <input name="elements" type="hidden">
                                          <i class="dropdown icon"></i>
                                          <div class="default text"></div>
                                          <div class="menu">
                                              @foreach ($sources as $tag)
                                                  <div class="item" data-value="{{$tag->id}}">{{$tag->name}}</div>
                                              @endforeach
                                          </div>
                                          
                                        </div>
                                        <div class="ui button mt-4" onclick="$('#dropdown').dropdown('clear')">
                                          Effacer la source
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col">
                                  <div class="form-group">
                                    <h3>Signature metreur</h3>
                                    <canvas id="mettreurSign" style="border: 1px solid black"></canvas>
                                    <div>
                                      <button type="button" id="metteurClear" class="btn btn-warning">Effacer</button>
                                    </div>
                                  </div>
                                </div>
                                <div class="col">
                                  <div class="form-group position-relative">
                                    <h3>Signature client</h3>
                                    <canvas id="clientSign" style="border: 1px solid black"></canvas>
                                    <div>
                                      <button type="button" id="clientClear" class="btn btn-warning">Effacer</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <button type='submit' class="btn btn-success btn-round mt-4">Créer la fiche client</button>
                          </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
<script src="{{asset('js/docusign/dist/signature_pad.min.js?ver=1.3')}}"></script>
<script src="{{asset('js/Semantic/semantic.min.js?ver=1.3')}}"></script>
<script src="{{asset('js/functions.js?ver=1.3')}}"></script>
<script src="{{asset('js/tables.js?ver=1.3')}}"></script>
<script src="{{asset('js/fiche-client.js?ver=1.3')}}"></script>
@endsection
