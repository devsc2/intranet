<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_prospect')->unsigned()->index();
            $table->string('name')->nullable();
            $table->string('extension');
            $table->string('path');
            $table->string('size')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        Schema::table('prospect_documents', function (Blueprint $table){
            $table->foreign('id_prospect')->references('id')->on('prospects')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_documents');
    }
}
