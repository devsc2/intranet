<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFournisseursProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fournisseurs_produits', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_fournisseur')->unsigned()->index()->nullable();
            $table->integer('id_type')->unsigned()->index()->nullable();
            $table->integer('id_version')->unsigned()->index()->nullable();
            $table->integer('id_produit_type')->unsigned()->index()->nullable();
            $table->integer('id_produit_type_param')->unsigned()->index()->nullable();
            $table->integer('id_dimension')->unsigned()->index()->nullable();
            $table->string('référence');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->unique(['id_fournisseur', 'id_type', 'id_version', 'id_produit_type', 'id_produit_type_param', 'id_dimension', 'référence'], 'unique_entries');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_fournisseur')->references('id')->on('fournisseurs');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_type')->references('id')->on('fournisseurs_types');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_version')->references('id')->on('fournisseurs_version');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_produit_type')->references('id')->on('fournisseurs_produits_types');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_produit_type_param')->references('id')->on('fournisseurs_produits_types_params');
        });

        Schema::table('fournisseurs_produits', function (Blueprint $table){
            $table->foreign('id_dimension')->references('id')->on('fournisseurs_produits_dimensions');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fournisseurs_produits');
    }
}
