<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectHomeStatusRequirementsParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_home_status_requirements_params', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_require')->unsigned()->index();
            $table->string('name');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->unique(['id_require', 'name'], 'unique_entries');
        });

        Schema::table('prospect_home_status_requirements_params', function (Blueprint $table){
            $table->foreign('id_require')->references('id')->on('prospect_home_status_requirements')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_home_status_requirements_params');
    }
}
