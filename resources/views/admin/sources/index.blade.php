@extends('layouts.app')
@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Ici vous pouvez gérer tous les sources. Bien sûr, seulement les administrateurs ont accés à cette page. Vous pouvez modifier, supprimer, créer et même modifier l\'ordre des sources !'),
        'img' => 'folder-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                        <div class="align-items-center">
                            @if (\Session::has('success'))
                                <div class="alert alert-success" role="alert">
                                    {!! \Session::get('success') !!}
                                </div>
                            @elseif(\Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    {!! \Session::get('error') !!}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    <h1>La liste des sources</h1>
                                    <p class="text-danger">ATTENTION ! Supprimer une source, supprimera toutes les fiches qui ont la source  supprimé !</p>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#addSourceModal">Ajouter une source</button>
                                    <div class="modal fade text-left" id="addSourceModal" tabindex="-1" role="dialog" aria-labelledby="addSourceModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.sources.store')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="addSourceModalLabel">Ajouter une source</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <input type="text" name="name" id="sourceName" placeholder="Nom de la source" class="form-control" required>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Créer la source</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="card-body">
                        <table class="table w-100 dt-table responsive display nowrap" id='table'>
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-left">#</th>
                                    <th class="text-left">Sources</th>
                                    <th class="text-right" width="15%">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($sources as $source)
                                    <tr>
                                        <th class='handle text-left'>{{$source->id}}</th>
                                        <th class='handle text-left'>{{$source->name}}</th>
                                        <th class="text-right">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#source{{$source->id}}EditModal">
                                                <i class="fas fa-user-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#source{{$source->id}}DeleteModal">
                                                <i class="fas fa-user-times"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    {{-- Edit modal --}}
                                    <div class="modal fade text-left" id="source{{$source->id}}EditModal" tabindex="-1" role="dialog" aria-labelledby="source{{$source->id}}EditModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.sources.update', $source->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('put')
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="source{{$source->id}}EditModalLabel">Modifier la source {{$source->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                        <label for="source{{$source->id}}Name" class="form-control-label">Nom :</label>
                                                        <input type="text" name="name" id="source{{$source->id}}Name" value="{{$source->name}}" class="form-control">
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                                  </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- Delete modal --}}
                                    <div class="modal fade text-left" id="source{{$source->id}}DeleteModal" tabindex="-1" role="dialog" aria-labelledby="source{{$source->id}}DeleteModalLabel" aria-hidden="true">
                                        <form action="{{route('admin.sources.destroy', $source->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('delete')
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="source{{$source->id}}DeleteModalLabel">Supprimer définitivement le sources {{$source->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p class="text-danger">Attention ! Vous êtes sur le point de supprimer définitvement le sources {{$source->name}}, êtes vous sûr ?</p>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-danger">Supprimer</button>
                                                  </div>
                                                </div>
                                              </div>
                                        </form>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="{{asset('js/tables.js?ver=1.1')}}"></script>
@endsection
