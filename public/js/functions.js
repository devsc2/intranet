var object = { 
    id_fournisseur: $('#id_fournisseur').val(), 
    id_type: $('#id_type').val(), 
    id_version: $('#id_version').val(),
    id_produit_type: $('#id_produit_type').val(),
    id_produit_type_param: $('#id_produit_type_param').val(),
    id_dimension: $('#id_dimension').val()
};

var source;

$('#dropdown').dropdown({
    allowAdditions: true,
    onChange: function(id, val){
        source = val;
    }
});
var [mettreurSign, clientSign] = [document.getElementById("mettreurSign"), document.getElementById("clientSign")];

if(mettreurSign) var metteurSignPad = new SignaturePad(mettreurSign);
if(clientSign) var clientSignPad = new SignaturePad(clientSign);

$("#metteurClear").on('click', function(){
    metteurSignPad.clear();
})
$("#clientClear").on('click', function(){
    clientSignPad.clear();
})

const getDimensions = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/prospect/dimensions',
            method: 'POST',
            data: object,
            success: (res) => {
                $("#id_dimension").empty();
                $('#id_dimension').append(`
                // <option value="0">Ne pas choisir de référence</option>
                // `)
                Object.entries(res).forEach(([key, value]) => {
                    $('#id_dimension').append(`
                    <option value="${key}">${value}</option>
                    `);
                });
                object.id_dimension = $('#id_dimension').val();
                resolve();
            }
        })
    });
}

const getInfoFromServer = () => {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/prospect/getInfos',
        type: 'POST',
        data: object,
        dataType: 'json',
        success: (res) => {
            $('#getReferences').empty()
            if(res.length > 0){
                $('#getReferences').append(`
                    <tr>
                        <input type="hidden" name="id_reference" value="${res[0].id}">
                        <th class="text-center">
                            ${res[0].id}
                        </th>
                        <th class="text-center">
                            ${res[0].référence}
                        </th>
                    </tr>
                `)
            }else{
                $('#getReferences').append(`
                    <input type="hidden" name="id_reference">
                `)
            }
            
        }
    })
}

const addDate = id_table => {
    const [
        date_name, 
        date_appel, 
        date_intervention, 
        date_assist
    ] = [
            $('#dateName').val(),
            $("#startDate").val(), 
            $("#endDate").val(), 
            $("#assistDate").val()
        ];
    if(date_name !== "" && date_appel !== "" && date_intervention !== ""){
        $("#date_alert").css({"display": "none"});
        dates[date_id] = {date_name: date_name, date_appel: date_appel, date_intervention: date_intervention, date_assist: date_assist}
        tables[id_table].row.add([
            date_name,
            date_appel,
            date_intervention,
            date_assist,
            `<button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm" data-original-title="" title="" id="delete${date_id}" onclick="deleteDate(${date_id}, '${id_table}')">
            <i class="ni ni-fat-remove pt-1"></i>
            </button>`
        ]).node().id = date_id;
        tables[id_table].draw(false);
        date_id = date_id + 1;

    }else{
        $("#date_alert").css({"display": "block"});
    }
}

const deleteDate = (id, id_table) => {
    dates.forEach((element, key) => {
        if(key > id){
            $(`#${key}`).attr('id', key - 1);
            $(`#delete${key}`).attr('onclick', `deleteDate(${key - 1}, 'EditDatesTable')`);
            $(`#delete${key}`).attr('id', `delete${key - 1}`);
        }
    })
    tables[id_table].row(id).remove().draw(false);
    dates.splice(id, 1);
    date_id = date_id - 1;
}

getDimensions().then(() => {
    getInfoFromServer();
});

Object.entries(object).forEach(([key, element]) => {
    document.getElementById(key).addEventListener('change', evt => {
        object[key] = document.getElementById(key).value;
        if(object["id_fournisseur" ] == 2){
            $("#decomposeParams").css({"display": "flex"});
            $("#kindeoParams").css({"display": "none"});
        }else{
            $("#kindeoParams").css({"display": "flex"});
            $("#decomposeParams").css({"display": "none"});
        }
        if(key == "id_produit_type"){
            getDimensions().then(() => {
                getInfoFromServer();
            });
        }else{
            getInfoFromServer();
        }
        
    })
})
var i = 0;
function addPhoto(key){
    i = i + 1;
    if(i < 5){
        $(`#col${key}`).append(`
        <div class="form-group">
            <div class="input-group mb-4">
                <h4>PHOTO ${i}</h4>
                <input type="file" class="form-control-file" name="requirementsFile[PHOTOS${i}]">
            </div>
        </div>
    `)
    }
}
