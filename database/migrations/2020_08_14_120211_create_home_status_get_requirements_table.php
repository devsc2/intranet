<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeStatusGetRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_status_get_requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_home_status')->unsigned()->index();
            $table->integer('id_require')->unsigned()->index();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->unique(['id_require', 'id_home_status'], 'unique_entries');
        });

        Schema::table('home_status_get_requirements', function (Blueprint $table){
            $table->foreign('id_require')->references('id')->on('prospect_home_status_requirements')->onDelete('cascade');
        });

        Schema::table('home_status_get_requirements', function (Blueprint $table){
            $table->foreign('id_home_status')->references('id')->on('prospect_home_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_status_get_requirements');
    }
}
