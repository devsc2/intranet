const radioValues = {
    status: $('#statusValue').val(),
    stairs: $('#stairsValue').val(),
    devisAnnexe: $('#devisAnnexeValue').val()
}


Object.entries(radioValues).forEach(([key, elements]) => {
    [...document.getElementsByName(key)].forEach(input => {
        if(input.value == elements){
            input.setAttribute('checked', true);
        }
    });
});

var dates = JSON.parse(document.getElementById('dateList').value);

dates.forEach((elements, key) => {
    tables.EditDatesTable.row.add([
        elements.date_name,
        elements.date_appel,
        elements.date_intervention,
        elements.date_assist,
        `<button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm" data-original-title="" title="" id="delete${key}" onclick="deleteDate(${key}, 'EditDatesTable')">
        <i class="ni ni-fat-remove pt-1"></i>
        </button>`
    ]).node().id = key;
    delete elements.id;
})

tables.EditDatesTable.draw(true);

function updateSigns(id, canvas, name){

    if(!canvas.isEmpty()){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: `/prospect/updateSigns/${id}`,
            data: {"base64": canvas.toDataURL(), name: name},
            success: () => {
                window.location.reload();
            }
        })
    }else{
        alert('Votre signature ne doit pas être vide');
    }
}

var date_id = dates.length;

$("#myForm").on('submit', function(e) {
    e.preventDefault();
    $("#ficheSuccess").css({"display": "none"});
    const formData = new FormData($('#myForm')[0]);
    $("#checkboxAlert").css({"display": "none"});
    formData.set('dates', JSON.stringify(dates));
    $("#date_alert").css({"display": "none"});
    formData.set('source', source);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        cache: false,
        url: `/prospect/edit/${document.getElementById('idEdit').value}`,
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: res =>{
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            $("#ficheSuccess").css({"display": "none"});
            $('#ficheError').css({"display": "none"});
            if(res == true){
                $("#ficheSuccess").css({"display": "block"});
            }else{
                $("#ficheError").html(res);
                $('#ficheError').css({"display": "block"});
            }
        }
    });
});


