<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = array(
            ["name" => "CREATION LEADS", 'order' => 1],
            ["name" => "METRAGE A PROGRAMMER", 'order' => 2],
            ["name" => "METRAGE EFFECTUER", 'order' => 3],
            ["name" => "RDV EXPERTISE POSITIONNE", 'order' => 4],
            ["name" => "DOSSIER EN COURS", 'order' => 5],
            ["name" => "A SAISIR", 'order' => 6],
            ["name" => "FINALISES", 'order' => 7],
            ["name" => "PB CONNEXION", 'order' => 8],
            ["name" => "ATTENTE PIECES", 'order' => 9],
            ["name" => "SIGNATURE CONVENTION", 'order' => 10],
            ["name" => "ACOMPTE DEBLOQUE", 'order' => 11],
            ["name" => "DEVIS ANNEXE", 'order' => 12],
            ["name" => "COMMANDE MATERIEL", 'order' => 13],
            ["name" => "RECEPTION MATERIEL", 'order' => 14],
            ["name" => "PROGRAMMATION INSTALLATION", 'order' => 15],
            ["name" => "EDITION FACTURE", 'order' => 16],
            ["name" => "DEBLOQUAGE SOLDE", 'order' => 17],
            ["name" => "CONTRÔLE QUALITE TEL", 'order' => 18],
            ["name" => "CONTRÔLE QUALITE ERGO", 'order' => 19],
            ["name" => "SANS SUITE", 'order' => 20],
        );

        DB::table('status')->insert($array);

        $home_status = array(
            ["name" => "PROPRIETAIRE"],
            ["name" => "LOCATAIRE"],
            ["name" => "HEBERGEE DEMANDEUR"],
            ["name" => "HEBERGEE DESCENDANT"],
            ["name" => "VIAGER"],
            ["name" => "USUFRUIT"],
        );

        DB::table('prospect_home_status')->insert($home_status);

        $home_status_requirements = array(
            ["name" => "CNI"],
            ["name" => "AVIS D'IMPOT"],
            ["name" => "TF"],
            ["name" => "AUTORISATION DU PROPRIETAIRE"],
            ["name" => "BAIL COMPLET"],
            ["name" => "ATTESTION D'HEBERGEMENT ECRITE PAR LE DESCENDANT"],
            ["name" => "DERNIER BULETIN DE SALAIRE"],
            ["name" => "TAXE FONCIERE OU ATTESTATION NOTAIRE"],
            ["name" => "ACTE NOTARIAL"],
            ["name" => "FICHE METRAGE"],
            ["name" => "PLAN SDB"],
            ["name" => "PHOTOS"],
            ["name" => "JUSTIFICATIF CARSAT"],
            ["name" => "AUTORISATION CREATION MAIL"],
            ["name" => "AUTORISATION ACCES AUX DONNEES"],
        );

        DB::table('prospect_home_status_requirements')->insert($home_status_requirements);

        $home_status_requirements_params = array(
            // CNI
            ["id_require" => 1, "name" => "RECTO MR"],
            ["id_require" => 1, "name" => "VERSO MR"],
            ["id_require" => 1, "name" => "RECTO MD"],
            ["id_require" => 1, "name" => "VERSO MD"],
            // AVIS D'IMPOT
            ["id_require" => 2, "name" => "VOLET 1"],
            ["id_require" => 2, "name" => "VOLET 2"],
            ["id_require" => 2, "name" => "VOLET 3"],
            ["id_require" => 2, "name" => "VOLET 4"],
            // TF
            ["id_require" => 3, "name" => "RECTO"],
            ["id_require" => 3, "name" => "VERSO"],
        );

        DB::table('prospect_home_status_requirements_params')->insert($home_status_requirements_params);

        $home_status_get_requirements = array(
            // Propriétaire
            ['id_home_status' => 1, 'id_require' => 1],
            ['id_home_status' => 1, 'id_require' => 2],
            ['id_home_status' => 1, 'id_require' => 3],
            ['id_home_status' => 1, 'id_require' => 10],
            ['id_home_status' => 1, 'id_require' => 11],
            ['id_home_status' => 1, 'id_require' => 12],
            ['id_home_status' => 1, 'id_require' => 13],
            ['id_home_status' => 1, 'id_require' => 14],
            ['id_home_status' => 1, 'id_require' => 15],
            // Locataire
            ['id_home_status' => 2, 'id_require' => 1],
            ['id_home_status' => 2, 'id_require' => 2],
            ['id_home_status' => 2, 'id_require' => 4],
            ['id_home_status' => 2, 'id_require' => 5],
            ['id_home_status' => 2, 'id_require' => 10],
            ['id_home_status' => 2, 'id_require' => 11],
            ['id_home_status' => 2, 'id_require' => 12],
            ['id_home_status' => 2, 'id_require' => 13],
            ['id_home_status' => 2, 'id_require' => 14],
            ['id_home_status' => 2, 'id_require' => 15],
            // Hebergee demandeur
            ['id_home_status' => 3, 'id_require' => 1],
            ['id_home_status' => 3, 'id_require' => 2],
            ['id_home_status' => 3, 'id_require' => 6],
            ['id_home_status' => 3, 'id_require' => 10],
            ['id_home_status' => 3, 'id_require' => 11],
            ['id_home_status' => 3, 'id_require' => 12],
            ['id_home_status' => 3, 'id_require' => 13],
            ['id_home_status' => 3, 'id_require' => 14],
            ['id_home_status' => 3, 'id_require' => 15],
            // Hebergee descendant
            ['id_home_status' => 4, 'id_require' => 7],
            ['id_home_status' => 4, 'id_require' => 8],
            ['id_home_status' => 4, 'id_require' => 10],
            ['id_home_status' => 4, 'id_require' => 11],
            ['id_home_status' => 4, 'id_require' => 12],
            ['id_home_status' => 4, 'id_require' => 13],
            ['id_home_status' => 4, 'id_require' => 14],
            ['id_home_status' => 4, 'id_require' => 15],
            // Viager
            ['id_home_status' => 5, 'id_require' => 1],
            ['id_home_status' => 5, 'id_require' => 2],
            ['id_home_status' => 5, 'id_require' => 3],
            ['id_home_status' => 5, 'id_require' => 9],
            ['id_home_status' => 5, 'id_require' => 10],
            ['id_home_status' => 5, 'id_require' => 11],
            ['id_home_status' => 5, 'id_require' => 12],
            ['id_home_status' => 5, 'id_require' => 13],
            ['id_home_status' => 5, 'id_require' => 14],
            ['id_home_status' => 5, 'id_require' => 15],
            // USUFRUIT
            ['id_home_status' => 6, 'id_require' => 1],
            ['id_home_status' => 6, 'id_require' => 2],
            ['id_home_status' => 6, 'id_require' => 3],
            ['id_home_status' => 6, 'id_require' => 9],
            ['id_home_status' => 6, 'id_require' => 10],
            ['id_home_status' => 6, 'id_require' => 11],
            ['id_home_status' => 6, 'id_require' => 12],
            ['id_home_status' => 6, 'id_require' => 13],
            ['id_home_status' => 6, 'id_require' => 14],
            ['id_home_status' => 6, 'id_require' => 15],
        );
        DB::table('home_status_get_requirements')->insert($home_status_get_requirements);

        $sources = array(
            ["name" => "Facebook"],
            ["name" => "Messenger"],
            ["name" => "Top optimisation"],
            ["name" => "Centre d'appel 2"],
            ["name" => "Mairie"],
        );

        DB::table('prospect_sources')->insert($sources);
    }
}
