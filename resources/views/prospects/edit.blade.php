@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('js/Semantic/semantic.min.css')}}">
@endsection
@section('content')    
@include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Vous pouvez modifier la fiche client !'),
        'img' => 'prospect-edit-cover.jpg'
    ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col mb-5 mb-xl-0">
                <div class="card bg-white">
                    <div class="card-header bg-white border-0">
                        @if (\Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {!! \Session::get('success') !!}
                            </div>
                        @elseif(\Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                              {!! \Session::get('error') !!}
                            </div>
                        @endif
                        <div class="col alert alert-success" id="ficheSuccess" role="alert" style="display: none">
                        Votre fiche client à été enregistrée avec succès !
                        </div>
                        <div class="col alert alert-danger" id="ficheError" role="alert" style="display: none">
                          
                        </div>
                        <div class="align-items-center">
                            <div class="row">
                                <div class="col">
                                    <h1>Créer une fiche client</h1>
                                </div>
                                <div class="col-md-5 text-right">
                                    <a href="{{route('read', $id)}}" class="btn btn-info" id='changeMode'>Mode read only</a>
                                    <button class="btn btn-info btn-round" type="button" data-toggle="modal" data-target="#changeDocumentsModal">Modifier les documents</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action='/' method='POST' id="myForm">
                            <h1 class="mb-4">Client :</h1>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="sexe" id="sexeMonsieur" value="Monsieur" class="custom-control-input" {{$data['sexe'] == 'Monsieur'?'checked':''}} required>
                                    <label class="custom-control-label" for="sexeMonsieur">Monsieur</label>
                                  </div>
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="sexe" id="sexeMadame" value="Madame" class="custom-control-input" {{$data['sexe'] == 'Madame'?'checked':''}} required>
                                    <label class="custom-control-label" for="sexeMadame">Madame</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Nom complet:</h3>
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Nom" name="full_name" required value="{{$data['pfullname']}}">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Adresse</h3>
                                <div class="form-group">
                                    <input placeholder="Adresse"class="form-control" name="address" value="{{$data['address']}}"/>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Code postal</h3>
                                <div class="form-group">
                                  <input type="number" name="zip_code" class="form-control" placeholder="Code postal" maxlength="5" value="{{$data['zip_code']}}" >
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Ville</h3>
                                <div class="form-group">
                                  <input type="text" name="city" value="{{$data['city']}}" placeholder="Ville" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Téléphone fixe :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Tél fixe" name="fixePhoneNumber" value="{{$data['home_phone']}}"/>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Téléphone portable :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <input type="text" class="form-control" placeholder="Tél portable" name="phoneNumber" value="{{$data['pPhoneNumber']}}">
                                    <div class="input-group-append">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <h1 class="mb-4">Référent :</h1>
                            <hr class="my-3">
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Nom</h3>
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Nom" value="{{$data['RLastname']}}" name="referentLastName">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Prénom</h3>
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Prénom" name="referentFirstName" value="{{$data['RFirstname']}}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Numéro de téléphone :</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                    </div>
                                    <input type="tel" class="form-control" placeholder="Téléphone" name="referentPhoneNumber" value="{{$data['RPhoneNumber']}}" maxlength="10"/>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <h3>Etat matrimonial</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <input type="text" name="referentStatus" class="form-control" value="{{$data['etat_matrimonial']}}">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <h3>Status dossier</h3>
                                <div class="form-group">
                                  <div class="input-group mb-4">
                                    <select class="form-control" id="exampleFormControlSelect1" name="folderStatus" style='cursor: pointer;'>
                                      @foreach ($status as $key => $item)
                                        @if ($item == $data['status'])
                                          <option value="{{$key}}" selected>{{$item}}</option>
                                        @else
                                        <option value="{{$key}}" >{{$item}}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <h3>Nom - Email de l'installateur</h3>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="form-control">
                                        {{$instas->name ?? ''}} - {{$instas->email ?? ''}}
                                      </div>
                                      <div class="input-group-prepend">
                                        <button class="btn btn-primary btn-round" type="button" data-toggle="modal" data-target="#changeInstaModal">Modifier</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <h3>Nom - Email de l'assistante sociale</h3>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="form-control">
                                        {{$assist->name ?? ''}} - {{$assist->email ?? ''}}
                                      </div>
                                      @if ($assist->id ?? 0 !== auth()->user()->id)
                                        <div class="input-group-prepend">
                                          <button class="btn btn-primary btn-round" type="button" data-toggle="modal" data-target="#changeAssistModal">Modifier</button>
                                        </div>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-10">
                                <h3>Status</h3>
                                <div class="form-group">
                                    <div class="input-group mb-4">
                                        <p class="form-control">{{$data['home_status']}}</p>
                                        <div class="input-group-prepend">
                                          <button class="btn btn-primary btn-round" type="button" data-toggle="modal" data-target="#changeStatusModal">Modifier le status</button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                              
                              <h3 class="mb-4">Escalier ?</h3>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="hidden" id="stairsValue" value="{{$data['stairs']}}">
                                <input type="radio" id="stairsRadio1" name="stairs" class="custom-control-input" value="Oui">
                                <label class="custom-control-label" for="stairsRadio1">Oui</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="stairsRadio2" name="stairs" class="custom-control-input" value="Non">
                                <label class="custom-control-label" for="stairsRadio2">Non</label>
                              </div>
                              <hr class="my-3">
                              <div class="row">
                                <div class="col-md-6">
                                  <h3>Mail AL</h3>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group mb-3">
                                        <p class="form-control" id="mail_al">{{$data['mail_al']}}</p>
                                        <div class="input-group-prepend">
                                          <button class="btn btn-primary btn-round" type="button" id="button-addon3" data-toggle="modal" data-target="#exampleMailModal">Modifier</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <h3>Password AL</h3>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group mb-3">
                                        <p class="form-control" id="pwd_al">{{$data['password_al']}}</p>
                                        <div class="input-group-prepend">
                                          <button class="btn btn-primary btn-round" type="button" id="button-addon1" data-toggle="modal" data-target="#exampleModal">Modifier</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="table-responsive overflow-hidden">
                                <table id="EditRefTable" class="table table-striped table-bordered w-100 dt-table-false responsive display nowrap">
                                    <thead>
                                      <tr>
                                          <th class="text-center">#</th>
                                          <th class="text-center">Fournisseur</th>
                                          <th class="text-center">Type de référence</th>
                                          <th class="text-center">Type de verre</th>
                                          <th class="text-center">Type de porte</th>
                                          <th>Dimension</th>
                                          <th class="text-center">Référence</th>
                                          <th class="text-center" width="10%">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @if(!empty($ref->id))
                                      <tr>
                                          <td class="text-center">{{$ref->id}}</td>
                                          <td class="text-center">{{$ref->fournisseur}}</td>
                                          <td class="text-center">{{$ref->type}}</td>
                                          <td class="text-center">{{$ref->Pparams}}</td>
                                          <td class="text-center">{{$ref->ProduitType}}</td>
                                          <td class="text-center">{{$ref->dimension}}</td>
                                          <td class="text-center">{{$ref->ref}}</td>
                                          <td class="text-center">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editReferenceModal">
                                              Modifier
                                            </button>
                                          </td>
                                      </tr>
                                      @endif
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editReferenceModal">
                                  Modifier
                                </button>
                              </div>
                              <hr class="my-3">
                              <h3 class="mb-4">Devis annexe ?</h3>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="hidden" id="devisAnnexeValue" value="{{$data['devis']}}">
                                <input type="radio" id="devisRadio1" name="devisAnnexe" class="custom-control-input" value="Oui">
                                <label class="custom-control-label" for="devisRadio1">Oui</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="devisRadio2" name="devisAnnexe" class="custom-control-input" value="Non">
                                <label class="custom-control-label" for="devisRadio2">Non</label>
                              </div>
                              <div class="row mt-4">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descriptif ..." name="descriptif">{{$data['descriptif']}}</textarea>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" placeholder="Commentaire ..." name="commentary">{{$data['commentaire']}}</textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <hr class="my-3">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="alert alert-danger" id="date_alert" role="alert" style="display: none">
                                    Votre date est vide
                                  </div>
                                </div>
                              </div>
                              <h1 class="mb-4">Dates :</h1>
                              <table id="EditDatesTable" class="table table-striped table-bordered w-100 dt-table-false responsive display nowrap">
                                  <thead>
                                      <tr>
                                          <th class="text-center">Nom</th>
                                          <th class="text-center">DATE APPEL</th>
                                          <th class="text-center">DATE INTERVENTION</th>
                                          <th class="text-center">DATE ASSISTANTE SOCIALE</th>
                                          <th class="text-right">Actions</th>
                                      </tr>
                                  </thead>
                                  <tbody></tbody>
                              </table>
                              <div class="row">
                                <input type="hidden" id="dateList" value="{{json_encode($dates)}}">
                                <div class="col-md-6">
                                  <label for="dateName" class="form-control-label">Nom date</label>
                                  <div class="form-group">
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                      </div>
                                      <input type="text" class="form-control" name="dateName" id="dateName"/>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <label for="startDate" class="form-control-label">Date appel</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <input class="form-control" placeholder="Start date" id="startDate" name="startDate" type="date"  pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                
                                <div class="col-md-6">
                                  <label for="endDate" class="form-control-label">Date intervention</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                         <input class="form-control" placeholder="End date" id="endDate" name="endDate" type="date" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"> 
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <label for="endDate" class="form-control-label">Date assistance sociale</label>
                                  <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                         <input class="form-control" id="assistDate" name="assistDate" type="date" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"> 
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                      <button type='button' class="btn btn-primary btn-round d-flex justify-content-between align-items-center" onclick="addDate('EditDatesTable')"><i class="ni ni-cloud-upload-96 mr-2"></i> 
                                        Ajouter une date
                                      </button>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="ui sub header">Source</div>
                                      <div class="ui fluid search selection dropdown" id='dropdown'>
                                          <input name="source" type="hidden">
                                          <i class="dropdown icon"></i>
                                          <div class="default text">Source</div>
                                          <div class="menu">
                                              @foreach ($sources as $tag)
                                                  <div class="item" data-value="{{$tag->id}}">{{$tag->name}}</div>
                                              @endforeach
                                          </div>
                                          
                                        </div>
                                        <div class="ui button mt-4" onclick="$('#dropdown').dropdown('clear')">
                                          Effacer la source
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col">
                                  <div class="form-group">
                                    <h3>Signature metreur</h3>
                                    @if(!file_exists(storage_path('app/public/prospects/requirements/'.$id.'/Signatures/metreur/metreur.png')))
                                    @hasroles(['admin', 'user'])
                                      <canvas id="mettreurSign" style="border: 1px solid black"></canvas>
                                      <div>
                                        <button type="button" id="metreurSave" class="btn btn-success" onclick="updateSigns({{$id}}, metteurSignPad, 'metreur')">Sauvegarder</button>
                                        <button type="button" id="metteurClear" class="btn btn-warning">Effacer</button>
                                      </div>
                                    @endhasrole
                                    @else
                                    <img src="{{asset('storage\\prospects\\requirements\\'.$id.'\\Signatures\\metreur\\metreur.png')}}">
                                    @endif
                                    
                                  </div>
                                </div>
                                <div class="col">
                                  <div class="form-group position-relative">
                                    <h3>Signature client</h3>
                                    @if(!file_exists(storage_path('app/public/prospects/requirements/'.$id.'/Signatures/client/client.png')))
                                    @hasroles(['admin', 'user'])
                                      <canvas id="clientSign" style="border: 1px solid black"></canvas>
                                      <div>
                                        <button type="button" id="clientSave" class="btn btn-success" onclick="updateSigns({{$id}}, clientSignPad, 'client')">Sauvegarder</button>
                                        <button type="button" id="clientClear" class="btn btn-warning">Effacer</button>
                                      </div>
                                    @endhasrole
                                    @else
                                    <img src="{{asset('storage\\prospects\\requirements\\'.$id.'\\Signatures\\client\\client.png')}}">
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <input type="hidden" id="idEdit" value="{{$id}}">
                              <button type='submit' class="btn btn-success btn-round mt-4">Modifier la fiche client</button>
                          </form>
                          <form action="{{route('updatePwd', $id)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modifier le mot de passe</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group">
                                          <div class="input-group mb-4">
                                            <input type="password" class="form-control" autocomplete="new-password" placeholder="Mot de passe AL" name="passwordAL" required>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                          <form action="{{route('updateMail', $id)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="modal fade" id="exampleMailModal" tabindex="-1" aria-labelledby="exampleMailModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleMailModalLabel">Modifier le mail</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group">
                                          <div class="input-group mb-4">
                                            <input type="email" class="form-control" autocomplete="new-password" value="{{$data['mail_al']}}" name="mailAL" required>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                          <div class="modal fade" id="editReferenceModal" tabindex="-1" aria-labelledby="editReferenceModalLabel" aria-hidden="true">
                            <form action="{{route('updateReference', $id)}}" method="post">
                              {{ csrf_field() }}
                              @method('put')
                              <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="editReferenceModalLabel">Modifier la référence</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <h1 class="mb-4">Fournisseur :</h1>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <div class="input-group mb-4">
                                              <select class="form-control fournisseur-elements" id="id_fournisseur" required="" style='cursor: pointer;'>
                                                @foreach ($fournisseurs as $key => $fournisseur)
                                                    <option value="{{$key}}">{{$fournisseur}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row" id="kindeoParams">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                <select class="form-control fournisseur-elements" id="id_type" required="" style='cursor: pointer;'>
                                                  @foreach ($fournisseurs_types as $key => $fournisseur_type)
                                                      <option value="{{$key}}">{{$fournisseur_type}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                <select class="form-control fournisseur-elements" id="id_version" required="" style='cursor: pointer;'>
                                                  @foreach ($fournisseurs_versions as $key => $fournisseur_version)
                                                      <option value="{{$key}}">{{$fournisseur_version}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                <select class="form-control fournisseur-elements" id="id_produit_type" required="" style='cursor: pointer;'>
                                                  @foreach ($fournisseurs_produits_types as $key => $fournisseur_produit_type)
                                                      <option value="{{$key}}">{{$fournisseur_produit_type}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                <select class="form-control fournisseur-elements" id="id_produit_type_param" style='cursor: pointer;'>
                                                  @foreach ($fournisseurs_produits_types_params as $key => $fournisseur_produit_type_param)
                                                      <option value="{{$key}}">{{$fournisseur_produit_type_param}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                <select class="form-control fournisseur-elements" id="id_dimension" style='cursor: pointer;'>
                                                  
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="row" id="decomposeParams" style="display: none">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="decomposeFournisseur" class="form-control-label">Votre référence</label>
        
                                            <div class="input-group mb-4">
                                              <input type="text" name="decomposeFournisseur" class="form-control" id="decomposeFournisseur">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                        <div class="table-responsive overflow-hidden">
                                          <table id="list_table" class="table table-bordered w-100">
                                              <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Référence</th>
                                                </tr>
                                              </thead>
                                              <tbody id="getReferences"></tbody>
                                          </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                      <button type="submit" class="btn btn-primary">Modifier la référence</button>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                        </div>
                          @if ($assist->id ?? 0 !== auth()->user()->id)
                            <form action="{{route('updateficheInstall', ['id'=>$id,'idUser'=>$assist->id ?? 0])}}" method="post">
                              {{ csrf_field() }}
                              @method('put')
                              <div class="modal fade" id="changeAssistModal" tabindex="-1" aria-labelledby="changeAssistModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="changeAssistModalLabel">Modifier {{$assist->role ?? ''}}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col">
                                          <div class="form-group">
                                            <div class="input-group mb-4">
                                              <select class="form-control" id="instaChange" name="userChange" required="" style='cursor: pointer;'>
                                                  @foreach ($allAssist as $item)
                                                    <option value="{{$item->id}}">{{$item->name}} - {{$item->email}}</option>
                                                  @endforeach
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                      <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          @endif
                            <form action="{{route('updateficheInstall', ['id'=>$id,'idUser'=>$instas->id ?? 0])}}" method="post">
                              {{ csrf_field() }}
                              @method('put')
                              <div class="modal fade" id="changeInstaModal" tabindex="-1" aria-labelledby="changeInstaModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="changeInstaModalLabel">Modifier {{$instas->role ?? ''}}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col">
                                          <div class="form-group">
                                            <div class="input-group mb-4">
                                              <select class="form-control" id="InstaChange" name="userChange" required="" style='cursor: pointer;'>
                                                  @foreach ($installateurs as $item)
                                                      <option value="{{$item->id}}">{{$item->name}} - {{$item->email}}</option>
                                                  @endforeach
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                      <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          <form action="{{route('updateDocuments', $id)}}" method="post" enctype="multipart/form-data" files=true>
                            {{ csrf_field() }}
                            @method('put')
                            <div class="modal fade" id="changeDocumentsModal" tabindex="-1" aria-labelledby="changeDocumentsModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="changeDocumentsModalLabel">Les documents</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    @foreach ($docs as $key => $item)
                                    <div class="row">
                                      <div class="col" id="col{{$key}}">
                                        <h2>{{$item->require}}</h2>
                                        @if($item->params !== null)
                                          @foreach (explode(', ', $item->params) as $k => $param)
                                            <div class="form-group">
                                              <div class="input-group mb-4">
                                                  <label for="requirementsFile{{$k}}">{{$param}}</label>
                                                  <input type="file" class="form-control-file" id="requirementsFile{{$key.$k}}" name="requirementsFile[{{$item->require}}|{{$param}}]">
                                              </div>
                                            </div>
                                          @endforeach
                                        @else
                                          <div class="form-group">
                                            <div class="input-group mb-4">
                                                <input type="file" class="form-control-file" id="requirementsFile{{$key}}" name="requirementsFile[{{$item->require}}]">
                                            </div>
                                          </div>
                                        @endif
                                      </div>
                                      @if($item->require === 'PHOTOS')
                                        <div class="col-md-5 text-right">
                                        <button type="button" class="btn btn-primary" onclick="addPhoto({{$key}})">Ajouter une photo</button>
                                        </div>
                                      @endif
                                    </div>
                                      
                                    @endforeach
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" data-toggle="modal" data-target="#documentModal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                          
                          <form action="{{route('updateHomeStatus', $id)}}" method="post">
                            {{ csrf_field() }}
                            @method('put')
                            <div class="modal fade" id="changeStatusModal" tabindex="-1" aria-labelledby="changeStatusModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="changeStatusModalLabel">Changer le nom du status</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      @foreach ($home_status as $item)
                                        <div class="custom-control custom-radio mb-3 text-uppercase">
                                          <input name="home_status" class="custom-control-input" id="home_status_radio{{$item->id}}" type="radio" value="{{$item->id}}" required>
                                          <label class="custom-control-label" for="home_status_radio{{$item->id}}">{{$item->name}}</label>
                                        </div>
                                      @endforeach
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" data-toggle="modal" data-target="#documentModal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                          
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
<script src="{{asset('js/tables.js?ver=1.2')}}"></script>
<script src="{{asset('js/Semantic/semantic.min.js?ver=1.2')}}"></script>
<script src="{{asset('js/docusign/dist/signature_pad.min.js?ver=1.2')}}"></script>
<script src="{{asset('js/functions.js?ver=1.2')}}"></script>
<script src="{{asset('js/edit.js?ver=1.2')}}"></script>
@endsection